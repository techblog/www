---
title: "Helm Plugin for Secret Management"
subtitle: "Using OpenStack Barbican for Kubernetes Secrets"
date: 2019-04-08T09:00:00+02:00
author: Ricardo Rocha
tags: ["kubernetes", "helm", "openstack", "barbican"]
---

[Helm](https://helm.sh/) is a popular package manager for Kubernetes hosted by
the CNCF at incubation-level. Applications managed by Helm are defined as
Charts, allowing easy reuse, versioning and sharing.

The charts are published in repositories such as [stable](https://github.com/helm/charts/tree/master/stable)
with more than 250 charts. Others include the
[incubator](https://github.com/helm/charts/tree/master/incubator) or
[cern](https://gitlab.cern.ch/helm/charts/cern) where we keep charts for
internal components or other dependencies. The [helm hub](https://hub.helm.sh)
tries to keep track of what is available.

There are several ways to manage Secrets in Kubernetes, including the [Vault
plugin](https://github.com/hashicorp/vault-plugin-auth-kubernetes) for
a managed solution, Bitnami's [SealedSecrets](https://github.com/bitnami-labs/sealed-secrets) 
with a custom CRD or FutureSimple's [helm-secrets](https://github.com/futuresimple/helm-secrets).
The latter is similar to the plugin we will describe here, but relying on SOPS.

## Helm Barbican Plugin

[Barbican](https://wiki.openstack.org/wiki/Barbican) is the service for Secret
and Certificate management in OpenStack. It offers a REST API with functionality
implemented by backend plugins - including Vault.

At CERN we have a large OpenStack deployment fully integrated with our internal
Identity services, making it straighforward for users to rely on Barbican for secret
storage. This also makes it a good option for management of secrets on the more
than 350 Kubernetes clusters we run.

Some requirements / goals for this plugin:

* offer a gitops style solution, with encrypted secrets version controlled
  along the rest of the application configuration data
* allow usage of unchanged upstream helm charts
* provide good integration with existing helm commands install, upgrade, ...
 
To fullfill these requirements we decided to store the keys in Barbican and
use them to encrypt the secrets yaml files, so they can be safely pushed to
version control systems. The helm release name is used to map the keys to the
encrypted values files.

{{< figure src="../../img/post/helm-barbican-plugin-workflow.png"
      caption=""
      class="caption"
>}}

## Usage

The plugin can be installed and setup as follows:
```bash
helm plugin install https://github.com/cernops/helm-barbican
```
```bash
export OS_TOKEN=$(openstack token issue -c id -f value)
```

Here are the main plugin commands:
```bash
  edit        edit secrets
  install     wrapper for helm install, decrypting secrets
  lint        wrapper for helm lint, decrypting secrets
  upgrade     wrapper for helm upgrade, decrypting secrets
  view        decrypt and display secrets
```

`edit` and `view` provide an easy way to manage the contents of encrypted
files, making sure that no unencrypted data is left behind. The content is kept
base64 encoded and can be safely pushed to version control.
```bash
helm secrets edit secrets.yaml
param1: value1
param2:
  subparam3: value3
```
```bash
cat secrets.yaml 
7VucQvBprOEesX2cNOF0EMGZrlpRlse7exakDXZDDCTEsVIo5oAHjcRq7yJkowzXg/egpOAa+i/UVFM=
```
```bash
helm secrets view secrets.yaml
param1: value1
param2:
  subparam3: value3
```

The helm release name is required to fetch the encryption key. If not passed
explicitly using the `--name` parameter the current working directory is taken
instead (as in the case above).

The additional commands `install`, `upgrade` and `lint` provide a one to one
mapping with the corresponding helm commands, but handling the secrets in
a seamless way to the end user. The plain text contents are only kept temporarily
and in shared memory to be passed to the helm command.
```bash
helm secrets install stable/mariadb --name mariadb --namespace mariadb --values secrets.yaml
NAME:   mariadb
LAST DEPLOYED: Thu Apr  4 00:19:35 2019
NAMESPACE: mariadb
STATUS: DEPLOYED
...
```
```bash
helm ls
NAME   	REVISION	UPDATED                 	STATUS  	CHART         	
mariadb	1       	Thu Apr  4 00:19:35 2019	DEPLOYED	mariadb-5.11.0	
```

The remaining commands work in a similar way:
```bash
helm secrets upgrade mariadb stable/mariadb --values secrets.yaml
Release "mariadb" has been upgraded. Happy Helming!
```
```bash
helm secrets lint ./mariadb/ --values secrets.yaml
==> Linting ./mariadb/
1 chart(s) linted, no failures
```

There are two additional commands - `enc` and `dec` - offering low level
functionality for in-place encrypting and decrypting of the secret files.
They are usually not required and special care needs to be taken when using
them not to commit unencrypted contents accidentally.

## Future and Contributions

Things are changing quickly in the area of Kubernetes deployment management -
[Kustomize](https://github.com/kubernetes-sigs/kustomize) support was
added to kubectl in 1.14. But for now our users rely on plain manifests or
helm, which remains the most popular solution and does a very good job.

If you have OpenStack and run Kubernetes on top of OpenStack, this plugin might
be of use. Contributions in our [github
repository](https://github.com/cernops/helm-barbican) are very welcome.

