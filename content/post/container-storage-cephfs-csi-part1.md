---
title: "Container Storage and CephFS (Part 1): CSI CephFS"
subtitle: "Overview of CSI and the CephFS Driver"
date: 2018-06-29T11:34:03+02:00
author: Róbert Vašek
tags: ["cephfs", "containers", "csi", "kubernetes"]
---

At CERN we need to handle [large amounts of data](https://home.cern/about/updates/2017/07/cern-data-centre-passes-200-petabyte-milestone)
and often look for new solutions to simplify deployments and increase
performance. As we increase our reliance on container deployments this is an
area where we've recently worked towards these goals.

The most popular container solution is Kubernetes, but we also support Docker
Swarm and Mesos. Having each container orchestrator (CO) manage storage in its
own way is problematic for several reasons:

* Lack of standardization in this field resulted in re-implementing the same functionality for each CO, with various
degrees of support for different storage types
* More code means higher development and maintenance costs
* Different COs may require different storage parameters

Things are changing for the better with the Container Storage Interface, CSI.

In the first part of this series we'll cover the general overview of CSI with regards to Kubernetes,
followed by a description of the CephFS CSI plugin along with a sample usage.

## Container Storage Interface

The [Container Storage Interface (CSI)](https://github.com/container-storage-interface/spec/blob/master/spec.md) is the industry standard for cluster-wide storage plugins.

It defines the protocol between a CO and a plugin with the result being a standard way to provision, attach, mount and create a snapshot of a volume.
The plugin adheres to the protocol and is independent of the CO that controls it, meaning it is implemented only once for a particular storage system and _just&nbsp;works™_ on any CO supporting CSI.
[Some things are beyond the specification](https://github.com/container-storage-interface/spec/blob/master/spec.md#objective), such as POSIX conformance, plugin lifecycle management and packaging.
Those details are left out as implementation-specific and are in the hands of CO and plugin authors.

Version 0.1.0 of CSI was released in December 2017, followed by 0.2.0 in February 2018. As of the writing of this article,
version 0.3.0 introduced the concept of volume snapshots, volume expansion and topology awareness is up-ahead. Releases are scheduled every
three months, with the plan of releasing 1.0 by the end of 2018.

CSI can be deployed to a cluster in various configurations. In general, there's a controller and a node plugin where the controller plugin
would be run on a master host and the node plugin on all the node hosts where volumes may be published. Other configurations are also possible:

![alt text](../../img/post/csi-plugin-topology.png "CSI Plugin topology")

The spec [architecture section](https://github.com/container-storage-interface/spec/blob/master/spec.md#architecture) provides additional information.

The CO talks to plugins using gRPC, with each plugin having an endpoint (a UNIX socket) on which CSI services are exposed.

CSI offers [three kinds of services](https://github.com/container-storage-interface/spec/blob/master/spec.md#rpc-interface) (RPC interfaces):

* **Identity Service**: allows a CO to query for plugin's capabilities, health probes and other metadata. Must be implemented by
  both controller and node plugins
* **Controller Service**: handles creation, deletion and listing for volumes and volume snapshots. Must be implemented by the controller plugin
* **Node Service**: handles volume (un)staging and (un)publishing on a node, making it ready for workloads. Must be implemented by the node plugin

The capabilities of the services mentioned above are of course non-exhaustive and list only the most prominent features.

## CSI in Kubernetes

CSI development started in Kubernetes and that's where its support is most up-to-date, with other COs soon to follow.

There are two places to look out for CSI in Kubernetes: the *kubelet* itself, which delegates the Node Service; and the so called *sidecar containers*,
which delegate Controller and Identity services. Overall, they follow the same layout of connecting to the plugin's endpoint, listening for various Kubernetes
events upon which they create CSI requests, call plugin's RPCs and receive responses from it.

Looking further into each of the components, the sidecar containers include:

* **driver-registrar**: deployed within the same Pod as the plugin itself, usually as a *DaemonSet*. Its job is to register the node plugin using the *csi.volume.kubernetes.io/nodeid* annotation ([more info here](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/storage/container-storage-interface.md#kubelet-to-csi-driver-communication))
* **external-provisioner**: usually deployed as a *StatefulSet*. It watches for *PersistentVolumeClaim* objects and calls the plugin's *CreateVolume* and *DeleteVolume* for claims referencing a *StorageClass* corresponding to this driver ([more info here](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/storage/container-storage-interface.md#provisioning-and-deleting))
* **external-attacher**: also deployed as *StatefulSet*. It watches for *added, updated and deleted* events in *VolumeAttachment* objects and calls the plugin's *ControllerPublishVolume* and *ControllerUnpublishVolume* RPCs ([more info here](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/storage/container-storage-interface.md#attaching-and-detaching))

![alt text](../../img/post/csi-cluster.png "Kubernetes cluster with a CSI plugin")

The other part of CSI code is handled by the kubelet and lives in package *k8s.io/kubernetes/pkg/volume/csi* as a volume plugin. As it was mentioned earlier, it takes care of all per-node related business.
It implements (at least) the following interfaces:

* **Attacher/Detacher**: creates a *VolumeAttachment* object in its *Attach* method (this emits an event which is captured by *external-attacher*, possibly on another node).
  In its *MountDevice* method it may call plugin's *NodeStageVolume* RPC on the mounting node - given the plugin advertises *STAGE_UNSTAGE_VOLUME* capability
* **Mounter/Unmounter**: *SetUpAt* method calls plugin's *NodePublishVolume* RPC on the mounting node

Now, to actually make the mount from driver's container visible to the underlying node, Kubernetes will provide a _target path_ into which the CSI plugin
shall mount the requested volume. This path (or rather its parent) is mounted into driver's container with bidirectional propagation ([shared mount](https://lwn.net/Articles/690679/)),
which allows for propagation of mount and unmount kernel events.

The main component is the actual plugin, usually also deployed as a DaemonSet (along the driver-registrar). One important point is that for each plugin there needs to be an individual set of all three sidecar containers.

## CephFS CSI Driver

The CSI driver for CephFS can be found at [github.com/ceph/ceph-csi](https://github.com/ceph/ceph-csi).

Its main features include:

* Provisioning of new CephFS shares and mount of existing shares
* Support for both kernel and fuse clients (configurable), useful as they do
  not always have feature parity

Here's a quick example mounting an existing share (note *provisionVolume* is
False, and we pass a *rootPath*):
```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: csi-cephfs
provisioner: csi-cephfsplugin
parameters:
  monitors: "128.141.111.222:6789,128.141.111.223:6789"
  provisionVolume: "false"
  rootPath: "/volumes/_nogroup/2dc522a1-bd5f-4e84-a914-c53137296e0c"
  csiProvisionerSecretName: csi-cephfs-secret
  csiProvisionerSecretNamespace: default
  mounter: kernel
reclaimPolicy: Delete
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: csi-cephfs-pvc
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 5Gi
  storageClassName: csi-cephfs
```

When provisioning a new share, the driver also creates a dedicated CEPHX user
who has its access restricted to this share only - meaning it needs to be
configured with an admin credential to the Ceph cluster. Here's an example
(note we also provide the *pool* where the share should be created):
```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: csi-cephfs
provisioner: csi-cephfsplugin
parameters:
  monitors: "128.141.111.222:6789,128.141.111.223:6789"
  provisionVolume: "true"
  pool: cephfs_data
  csiProvisionerSecretName: csi-cephfs-secret
  csiProvisionerSecretNameSpace: default
  mounter: kernel
reclaimPolicy: Delete
```

More details about available config options and deployment can be [found here](https://github.com/ceph/ceph-csi/#cephfs-plugin).

Full demo of how to define all required Kubernetes resources and deploy them in an existing cluster:

<p><center><script src="https://asciinema.org/a/jMBLsoqQzIMScOvNqpY1QNzFy.js" id="asciicast-jMBLsoqQzIMScOvNqpY1QNzFy" async></script></center></p>

Mounting the share is the same as for any other PVC:

<p><center><script src="https://asciinema.org/a/nwlpY99eHvvFFNlFnadggKxUo.js" id="asciicast-nwlpY99eHvvFFNlFnadggKxUo" async></script></center></p>

Part 2 of this series will expand on auto provisioning of storage resources.

