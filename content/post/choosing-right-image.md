---
title: 'Choosing the right image'
date: 2015-02-05T05:30:00.002-08:00
draft: false
author: Tim Bell
tags: ["openinfra"]
---

Over the past 18 months of production at CERN, we have provided a number of standard images for the end users to use when creating virtual machines.  

*   Linux

*   Scientific Linux CERN 5
*   Scientific Linux CERN 6
*   CERN CentOS 7

*   Windows

*   Windows 7
*   Windows 8
*   Windows Server 2008
*   Windows Server 2012

To accelerate deployment of new VMs, we also often have

*   Base images which are the minimum subset of packages on which users can build their custom virtual machines using features such as cloud-init or Puppet to install additional packages
*   Extra images which contain common additional packages and accelerate the delivery of a working environment. These profiles are often close to a desktop like PC-on-demand. Installing a full Office 2013 suite on to a new virtual machine can take over one hour so preparing this in advance saves time for the users.

However, with each of these images and additional software, there is a need to maintain the image contents up to date.

*   Known security issues should be resolved within the images rather than relying on the installation of new software after the VM has booted
*   Installation of updates slows do the instantiation of virtual machines

The images themselves are therefore rebuilt on a regular basis and published to the community as public images. The old images, however, should not be deleted as they are needed in the event of a resize or live migration (see [https://review.openstack.org/#/c/90321/](https://review.openstack.org/#/c/90321/)). Images cannot be replaced in Glance since this would lead to inconsistencies on the hypervisors.

  

As a result, the number of images in the catalog increases on a regular basis. For the web based end user, this can make navigating the Horizon GUI panel for the images difficult and increase the risk that an out of date image is selected.

{{< figure src="../../img/post/choosingimages.png"
     caption="Image selection panel"
     class="caption"
>}}
  
The approach that we have taken is to build on the image properties (http://docs.openstack.org/cli-reference/content/chapter_cli-glance-property.html) which allow the image maintainer to tag images with attributes. We use the following from the standard list


*   architecture => "x86_64", "i686"

*   os_distro => "slc", "windows"

*   os\_distro\_major => "6", "5"

*   os\_distro\_minor => "4"

*   os_edition => "<Base|Extra>" which set of additional packages were installed into the image.

*   release_date => "2014-05-02T13:02:00" for at what date was the image made available to the public user

*   custom_name => "A custom name" allows a text string to override the default name (see below)

*   upstream_provider => URL gives a URL to contact in the event of problems with the image. This is useful where the image is supplied by a 3rd party and the standard support lines should not be used.

  
  

We also defined additional fields

  

With these additional fields, the latest images can be selected and a subset presented for the end user to choose.

{{< figure src="../../img/post/choosingimagelatest.png"
     caption="Selecting latest image"
     class="caption"
>}}


The algorithm used is as follows with the sorting sequence

  

1.  os_distro ASC
2.  os\_distro\_major DESC
3.  os\_distro\_minor DESC
4.  architecture DESC
5.  release_date DESC

Images which are from previous releases (i.e. where os\_distro, os\_distro_major and architecture are the same) are only shown if the 'All' tab is selected.

  

The code is in preparation to be proposed as an upstream patch. For the moment, it can be found in the CERN github repository (https://github.com/cernops/horizon)

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

