---
title: "OpenStack Placement requests - The Uphill and Downhill"
subtitle: "From 125k to 10k requests per minute"
date: 2018-12-07T11:28:46+01:00
author: Belmiro Moreira
tags: ["openstack"]
---

## Overview

In this blogpost I will describe the Placement scalability issue that we identified when 
OpenStack Nova was upgraded to Rocky and all the work done to mitigate this problem.

## OpenStack Placement
Placement is a service that tracks the inventory and allocations of resource
providers (usually compute nodes). The resource tracker runs in each compute node
and it sends the inventory and allocations information to Placement through a 
periodic task.

When a new instance is created the nova-scheduler asks placement for allocation
candidates. Placement retrieves allocation candidates for the new instance
considering the requested resource classes, the inventory and current allocations.
Then the nova-scheduler selects an allocation candidate (a compute node in the simplest case)
based on the enabled scheduler filters.

## OpenStack Placement at CERN

Placement service was introduced in Newton release and is required since Ocata release.
At CERN we are running the Placement service for several months, first when using CellsV1
and now with CellsV2. Placement is a global service. This means that all the resources
from the cloud infrastructure are tracked by this service. If it's unavailable,
it is not possible to schedule new instances in the Cloud Infrastructure. Placement
is a simple service that scales horizontally and is easy to have a fault
tolerant configuration. Simply have enough "nova-placement-api" services running
to handle the current load and some overcapacity just in case some of them fail.

At CERN, when we upgraded to Queens release the "nova-placement-api" service was
running in 16 servers (virtual machines with 4 vcpus/8GB of RAM) sharing the
resources with "nova-scheduler" and "nova-conductor".

The load in the placement service is predictable. If we don't consider the requests
from the "nova-scheduler" (that varies considering the VM creation rate
in the Cloud) the other requests are from the compute nodes resource tracker.
This means that the number of requests increases linearly considering the number
of compute nodes.

The CERN Cloud has more than 9k compute nodes.

## Nova upgrade to Rocky

Before the Nova Rocky upgrade we started to run "nova-placement-api" in 10 dedicated
virtual machines (4 vcpus/8GB of RAM) to be easier to handle dependencies during
upgrades and because Placement service is evolving as an independent service.
The number of the nodes was dimensioned to handle all the requests even if
two-thirds of placement nodes failed.

Before any Nova upgrade we test the new release in a small independent infrastructure
(just for curiosity we call it "testStack") to validate the release. We do several
functional tests to validate the features that we use and the integration with CERN
services. Unfortunately, the size of this test infrastructure doesn't allow us
to do scalability tests.

As best practice, before any Nova upgrade, we create several "hot standby" virtual
machines that can be configured to run any nova service. Remember that all our
Nova control plane runs in virtual machines hosted and managed by the same infrastructure
(inception!). If the infrastructure is not available we can't add more nodes in our
control plane.

The Nova upgrade to Rocky consisted in two phases. The upgrade of the Nova
control plane and then the upgrade of the compute nodes. We stage the deployment
of new packages per availability zone. A new package takes 3 days to reach all
the infrastructure. For more information about the Nova Rocky upgrade you can
watch the last slides of the presentation [Scaling Nova with CellsV2 The Nova Developer
and the CERN Operator perspective](https://www.youtube.com/watch?v=tNU5xkchTxw)
from the OpenStack Summit Berlin 2018.

### Placement requests boost

When the compute nodes started to be upgraded to Nova Rocky we started to notice
a huge increase in the number of requests to the Placement service. Fig. 1.

{{< figure src="../../img/post/placement-request-increase.png"
     caption="Fig. 1 - Increase of Placement requests after upgrade to Rocky"
     class="caption"
>}}

The load in the Placement servers increased several times.

When we detected this issue we need to use all our "hot standby" servers to increased in
number of Placement nodes. We needed to triple the number of nodes (30 Placement
servers) in order to handle all the load and have some spare capacity in case of
any server failures. Fig. 2.

{{< figure src="../../img/post/placement-request-time.png"
     caption="Fig. 2 - Request processing time before and after adding more placement nodes"
     class="caption"
>}}

Unfortunately, this also affected our log infrastructure
because the new millions of log lines that needed to be processed per day.

### The fix...

With the increase of placement requests we needed to scale up our Placement infrastructure.
Fortunately Placement scales horizontally. However, we started to question the reason
of this huge increase of Placement requests from the compute nodes.

We contacted the Nova and Placement teams to try understand the issue.

Turns out the increase of the number of requests is related to new validations
added in the resource tracker to maintain a local cache in the compute nodes
(the local resource provider tree). It was agreed that all these validations
were not required currently and can represent scalability issues when running Placement
in a large Infrastructure.

During the next days the Placement team proposed several patches to mitigate this
scalability issue with the reduction of validations required to keep the local
resource provider tree in the compute nodes.

For CERN specific use case, we started review the following patches:

* [Consolidate inventory refresh](https://review.openstack.org/#/c/615695/)
* [Reduce calls to placement from _ensure](https://review.openstack.org/#/c/615677/)
* [SIGHUP n-cpu to refresh provider tree cache](https://review.openstack.org/#/c/615646/)
* [Allow resource_provider_association_refresh=0](https://review.openstack.org/#/c/615606/)

Not all of then are merged yet at time of writing. However, we felt confident enough
to integrate them in our release and deploy them.

This is the result in the number of requests:

{{< figure src="../../img/post/placement-request-fix.png"
     caption="Fig. 3 - Total number of Plamcement requets"
     class="caption"
>}}

{{< figure src="../../img/post/placement-request-fix-type.png"
     caption="Fig. 4 - Number of Plamcement requets per type"
     class="caption"
>}}

# Wrapping up
The resource tracker in Nova Rocky does a huge number of validations with the Placement
service to keep the local resource provider tree consistent.

Placement really scales horizontally. After Nova Rocky upgrade the service was
handling ~125k requests per minute at CERN Cloud.

There are already patches proposed by the Placement team to reduce number of Placement
requests. At CERN we felt confident to deploy them into production in order to reduce the
load in the Placement service.

It's not clear if these patches will be backported to Rocky release.
If you are running a large infrastructure you may consider to cherry-pick them
into your internal release.

Finally we would like to thank Nova and Placement teams for all the help with this
scalability issue.

*Belmiro Moreira; Surya Seetharaman*
