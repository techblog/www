---
title: "Openstack Day CERN"
date: 2019-03-19T11:21:10+01:00
author: Belmiro Moreira
tags: ["openstack"]
---

{{< figure src="../../img/post/openstack-day-cern-globe.jpg"
>}}

CERN, the European Organization for Nuclear Research, is organizing an OpenStack Day (OSD) on May 27th, 2019. 

The event theme is _“Accelerating Science with OpenStack”_

In this event we would like to gather the OpenStack community for one day discussion on how OpenStack is helping thousands of scientists around the world. Talks from CERN, SKA and SWITCH are already confirmed!

There are opportunities for lightning talks on topics around the use of OpenStack in scientific domains. The deadline for the talk submission is March 31st, 2019. To submit an abstract, follow the detailed explanation available at: https://openstackdayscern.web.cern.ch/present

The day after, May 28th, there will the possibility to visit some of the CERN experiments. Don't forget to mention if you are interested to join a visit in the registration page.

Registration is available through eventbrite at: https://www.eventbrite.com/e/openstack-day-cern-tickets-54349067524  

Event details are available at: https://openstackdayscern.web.cern.ch/  
Let us know if you have any question: openstack-day-cern-contact at cern.ch

> _Join us for the OpenStack Day CERN. Space is limited!_  
_Hope to see you in Geneva!_

