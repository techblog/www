---
title: "OpenInfra User Group Meeting at CERN"
date: 2024-07-15T17:30:01+02:00
draft: false
author: Luis Fernandez Alvarez
tags: ["openinfra", "openstack", "ironic", "meetup"]
---

As part of the [OpenInfra Days Europe 2024](https://openinfra.dev/days), the
CERN IT cloud team had the pleasure of organising a two-day event, combining
a [Bare Metal SIG/Ironic Meetup](https://indico.cern.ch/event/1378171/) on 5th
June 2024 and an [OpenInfra User Group Meeting](https://indico.cern.ch/event/1376907/)
Switzerland on 6th June 2024. This event is the 3rd of this kind that CERN IT organizes
after the [OpenStack User Group](https://indico.cern.ch/event/273593/) in 2013
and the [OpenStack Day CERN](https://indico.cern.ch/event/776411/) in 2019.

The **Bare Metal SIG/Ironic Meetup** was a full day event chaired by Jay Faulkner
and Riccardo Pittau as the PTLs of the previous and the current development cycles.
The morning session was dedicated to discuss Ironic's history, features and deprecations.
You shouldn't miss Jay's talk about [the Ironic Toolbox](https://videos.cern.ch/record/2300308)
and Riccardo's presentation about [Metal3](https://videos.cern.ch/record/2300310).

{{< figure src="../../img/post/2024_openinfra_sig_room.jpg"
    caption="Participants of the Bare Metal SIG/Ironic Meetup"
    class="caption"
>}}

An open discussion session run through the whole afternoon. Topics ranged
from operational issues, feature wishlists, shaping of existing functionality
and future plans. You can check the fruits of this lively discussion in the
[event etherpad](https://etherpad.opendev.org/p/baremetal-sig-cern-june-5-2024).

A visit to the [CERN Data Centre](https://home.cern/science/computing/data-centre)
completed the event, together with a meetup dinner.

The day after, the **OpenInfra User Group Meeting** started. It welcomed more than
80 participants from 28 different organizations. The event served as a hub to
discuss common areas of interest, share experiences and foster collaboration
across the various representatives from academia, research institutions,
cloud providers, telecommunications, financial companies and other IT firms.

Participants were welcomed at the [CERN Science Gateway](https://visit.cern/) in 
the morning to participate in a series of visits to the
[Antimatter Factory](https://home.cern/science/physics/antimatter), the
[LINAC2](https://home.cern/science/accelerators/linear-accelerator-2) and the
[LEIR](https://home.cern/science/accelerators/low-energy-ion-ring).

{{< figure src="../../img/post/2024_openinfra_visits.jpg"
    caption="Visit groups during the OpenInfra event"
    class="caption"
>}}

After lunch, a series of presentations took place in the IT Amphitheatre. The session
started with a welcome talk by Enrica Porcari as the IT department head where she
highlighted the commitment of CERN with the various open source communities it contributes to
and the importance of keeping them alive and energized.

{{< figure src="../../img/post/2024_openinfra_welcome.png"
    caption="Enrica welcoming the participants of the OpenInfra event"
    class="caption"
>}}

Arne Wiebalck, head of the Compute and Devices group in CERN IT, gave an overview of
the role IT plays at CERN and the history of the CERN OpenStack private cloud - why it was
adopted, how it started and where it is now.

{{< figure src="../../img/post/2024_openinfra_cloud_journey.jpg"
    caption="Arne presenting the CERN Private Cloud Journey"
    class="caption"
>}}

The talks continued with José Castro León, technical lead of the CERN Cloud Service,
presented in detail the current status of their OpenStack deployment, the adoption
process of the new Prevessin Data Centre and the challenges ahead for the service.

{{< figure src="../../img/post/2024_openinfra_private_cloud.jpg"
    caption="José starting his talk about the CERN OpenStack private cloud"
    class="caption"
>}}

Mattia Belluco, Cloud Architect at the [Scientific IT Services of ETH Zurich](https://sis.id.ethz.ch/),
showed how they leverage OpenStack as an orchestration layer for their secured HPC service and
how they offer services like Kubernetes to their user community.

{{< figure src="../../img/post/2024_openinfra_eth_zurich.jpg"
    caption="Mattia presenting their infrastructure at ETH Zurich"
    class="caption"
>}}

This first block was completed by Axel Jacquet and Siméon Gourlin, Site Reliability Engineers at
[Infomaniak](https://www.infomaniak.com/). The presentation covered the design of their new
data centres and how the heat generated is being used to heat a residential neighborhood in
Switzerland. They presented as well the role OpenStack plays as the foundation of their public
cloud offering and what are the main challenges they are confronted with operating it.

{{< figure src="../../img/post/2024_openinfra_infomaniak.png"
    caption="Axel & Siméon about to start their presentation about the Infomaniak infrastructure"
    class="caption"
>}}

After this first block of presentations, a short coffee break gave participants the opportunity
to get some fresh air and start some discussions.

Right after coffee, Jay Faulkner, open source developer at [G-Research](https://www.gresearch.com/)
and former PTL of Ironic, changed the subject and the tempo of the event with an
energetic presentation about the impact of technology, giving a different prespective
to our job in the OpenInfra ecosystem.

{{< figure src="../../img/post/2024_openinfra_mundane.jpg"
    caption="Jay presenting 'Open Infrastructure: From Monumental to Mundane and Back Again'"
    class="caption"
>}}

The session was completed with three more technical presentations about Kubernetes,
GPUs and Networking.

Matt Pryor, Senior Tech Lead at [StackHPC](https://www.stackhpc.com/), presented
an overview of Cluster API and its benefits to manage Kubernetes clusters through
Magnum and Azimuth.

{{< figure src="../../img/post/2024_openinfra_capi.jpg"
    caption="Matt starting the overview of Cluster API"
    class="caption"
>}}


Sylvain Bauza, Principal Software Engineer at [Red Hat](https://www.redhat.com/)
and Nova/Placement PTL, shared the latest developments in Nova to offer virtual
GPUs in OpenStack to end-users. The presentation included hands-on recordings
of the different operations.

{{< figure src="../../img/post/2024_openinfra_vgpus.jpg"
    caption="Sylvain while showing vGPU live migration, one of the latest features in Caracal"
    class="caption"
>}}

Bernard Cafarelli, Associate Manager of Software Engineering at
[Red Hat](https://www.redhat.com/) closed the presentations with a comprehensive
overview of the work done in the OVN BGP agent and their plans for the future.

{{< figure src="../../img/post/2024_openinfra_ovn.jpg"
    caption="Bernard answering questions after his talk about the ovn-bpg-agent"
    class="caption"
>}}

If you want to know more details about any of the presentations, you will find
all the material availabe in the [Timetable](https://indico.cern.ch/event/1376907/timetable/#20240606)
section of the event site.

The presentations were followed by a group photo. Initially planned to be taken outdoors in front
of the data centre, weather conditions forced us to look for a plan B. The photo was
finally taken at the CERN Main Building:

{{< figure src="../../img/post/2024_openinfra_group.jpg"
    caption="Group photo of the participants of the OpenInfra User Group Meeting"
    class="caption"
>}}

A cocktail sponsored by CERN IT was offered to all the participants of the event,
a perfect opportunity to get a well deserved break after an intense day and to
initiate discussions of the many topics presented during the day.

We would like to thank all the speakers for their contribution,
the participants for taking the time to join us and contribute to
such a memorable day, and all the members of the organisation that
helped with the event.
