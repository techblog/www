---
title: "Backfilling Kubernetes Clusters"
subtitle: "Leveraging Pod Priority, Preemption and Boinc"
date: 2019-04-18T09:00:00+01:00
draft: true
author: Ricardo Rocha
tags: ["kubernetes", "docker", "boinc", "openstack"]
---

In addition to its large batch computing farm where resource usage is high,
CERN also has significant resources available to its internal services.
Typically these are not cpu intensive (though things are changing with the
growing popularity of tools like Spark, Kubeflow or Jupyter Notebooks) but
service managers are tempted to overprovision to count for sporadic load spikes. 

To try to optimize resource usage in this area we apply different methods:

* cpu overcommit, as seen in most private and public clouds
* preemptible vms (recent and ongoing work, [presented here](https://www.openstack.org/summit/berlin-2018/summit-schedule/events/22438/science-demonstrations-preemptible-instances-at-cern-and-bare-metal-containers-for-hpc-at-ska))

This gives an option to rely on virtualization to *fill the holes* and achieve
higher resource usage.


{{< figure src="../../img/post/priority-preemption-vm-comparison.png"
      caption=""
      class="caption"
>}}

As workloads move towards Kubernetes clusters, we will describe how we now have
an opportunity to improve these decisions relying on [Pod
priority and preemption](https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/) and [horizontal pod auto scaling](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/), and how we're looking into
backfilling clusters with physics workloads running on Boinc.

And with the simplicity of a helm chart, how **you can contribute to physics
research** with your own idle cycles! Read along...

### Meet Boinc

If you haven't heard of [Boinc](https://boinc.berkeley.edu/), it's the software
behind [SETI@home](https://setiathome.berkeley.edu/) and several other volunteer computing projects, including
CERN's [LHC@home](https://lhcathome.cern.ch/lhcathome/). Thousands of
people around the world offer their idle computing resources (typically
desktops/laptops and mobile phones) for these causes.

{{< figure src="../../img/post/priority-preemption-boinc-users-white.png"
      caption="LHC@home Number of Users"
      class="caption"
>}}

The LHC@home community has over 8000 contributors, with peaks of over 300
thousand concurrent tasks being run. This is a very significant contribution to
our computing needs.

{{< figure src="../../img/post/priority-preemption-boinc-tasks-white.png"
      caption="LHC@home Tasks with a peak of 300k"
      class="caption"
>}}

And as these workloads require no special authentication or authorization,
they're a good candidate to be deployed in untrusted clusters. 

### Priority and Preemption

[Priority and preemption](https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/) are essential on any large deployment accepting heterogeneous workloads.
These features were long awaited by our community and landed in Kubernetes 1.8
as alpha, being currently in beta and enabled by default.

Pods can be associated with priority classes, which have a name and a value.
Below are the priority classes we define by default in our clusters:
```bash
apiVersion: scheduling.k8s.io/v1beta1
kind: PriorityClass
metadata:
  name: default
value: 10000
globalDefault: true
---
apiVersion: scheduling.k8s.io/v1beta1
kind: PriorityClass
metadata:
  name: backfill
value: -1000
```

*default* is assigned to any Pod with no explicit priority class, and has
a value of 10000 (max is 1 billion). *backfill* has a much lower value ensuring
they will always get preempted by application workloads.

Two important things to be aware of that might save you some debug time:

* We use a negative value for the backfill priority workloads to stay best
  effort and prevent the kubernetes cluster auto scaler from triggering cluster
  scaling actions based on them. This is a major feature available since 1.9, and a bit
  [hidden in the
  docs](https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/FAQ.md#how-does-cluster-autoscaler-work-with-pod-priority-and-preemption).
  We will cover our usage of the cluster auto scaler in a later post
* If you wonder why your Pods have a priority class but are still scheduled
  without a priority value, you're likely missing *Priority* in your list of
  enabled admission controllers. Ours currently looks like this:

```sh
--admission-control=NodeRestriction,NamespaceLifecycle,LimitRanger,ServiceAccount,\
  DefaultStorageClass,DefaultTolerationSeconds,MutatingAdmissionWebhook,\
  ValidatingAdmissionWebhook,ResourceQuota,Priority
```

### Horizontal Pod Auto Scaling

To backfill the clusters with Boinc workloads we define both a *Deployment* and
a *Horizontal Pod Auto Scaler (hpa)*.

Here's the *Deployment* definition:
```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: boinc
  labels:
    app: boinc
spec:
  selector:
    matchLabels:
      app: boinc
  template:
    metadata:
      labels:
        app: boinc
    spec:
      priorityClassName: backfill
      containers:
      - name: boinc
        image: boinc/client
        env:
        - name: BOINC_CMD_LINE_OPTIONS
          value: "--attach_project MYPROJ MYKEY"
        resources:
          requests:
            cpu: 1000m
            memory: 256Mi
          limits:
            cpu: 1000m
            memory: 256Mi
```

which should not have anything surprising. Just note the priority
class set to *backfill* and that we limit the resources for each Boinc instance
to 1 core and 256MB memory. This gives a good granularity for the scheduler and
the pod auto scaler to keep the cluster full.

As for the *Horizontal Pod Auto Scaler*:
```bash
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: boinc
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: boinc
  minReplicas: 1
  maxReplicas: 1000000000
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 80
```
it has a few interesting things worth explaining:

* we scale based on *cpu*, and set the target to an average cpu utilization of
  80% across all Boinc pods. This seems to do the job of always keeping enough
  Boinc instances active to achieve close to 100% cpu usage across the cluster
* the value of *maxReplicas* is set to a *very unreasonable* high value. The
  reason is that we do not wish to have an upper limit - as long as the cluster
  has resources available to backfill, we should use them. [Here's a ticket](https://github.com/kubernetes/kubernetes/issues/75113)
  wondering if making *maxReplicas* optional makes sense

### Deployment and Results

We plan to start by offering backfill as an *opt-in* at cluster creation, via a
*backfill_enabled* flag. As we gain more confidence in this setup, we will turn
it on by default leaving it as *opt-out* to our users.

These charts show the measured CPU usage of both Pods and Nodes in a cluster
with backfill enabled. The deployment is managed by a [helm
chart](https://github.com/cernops/helm-boinc), check it out for the full
documentation.

{{< figure src="../../img/post/priority-preemption-pod-cpu-utilisation.png"
      caption="CPU Usage of Boinc backfill and high priority application Pods"
      class="caption"
>}}

The green area shows the usage of the *Boinc* workloads. The cluster starts
idle, so these backfill workloads quickly fill in the whole cluster.
```bash
kubectl -n boinc-hpa get hpa
NAME        REFERENCE          TARGETS   MINPODS   MAXPODS    REPLICAS   AGE
hpa/boinc   Deployment/boinc   99%/80%   1         10000000   7          16m
```

The vertical lines show the moments of change of the *high priority* application
replica number.

1. Setting replicas to 1 the corresponding CPU request is taken from *Boinc*
   which scales down (Pods are terminated)
2. Setting it to 5 right after most of the cluster resources are taken by the
   *High Priority* application
3. Scaling our application back to 1 replica *Boinc* scales up and takes over
   most of the resources

{{< figure src="../../img/post/priority-preemption-node-cpu-utilisation.png"
      caption=""
      class="caption"
>}}

This chart shows CPU usage in our test cluster of 2 nodes. As our backfill *Boinc*
launches right from the start we quickly get to 80% overall usage - scheduling is
disabled on the master node. The most interesting bit is realizing that this
usage level is kept even when Boinc Pods are being preempted and replaced with
our high priority application.

### Caveats & Future Work

This setup shows how otherwise challenging tasks can become
surprisingly simple when applications are defined as Kubernetes workloads. It
works well and we manage to achieve high resource usage on our clusters.

A few things to take into account:

* It is too efficient to be deployed blindly if you already overcommit on CPU -
  if you do this, expect tickets from unhappy neighbouring users :)
* Works best when backfill workloads can be broken into small pieces with low
  resource requests (such as the ones above requiring 1 cpu / 256MB). Anything
  big makes it hard to fit in the holes, we currently have this problem with
  our ATLAS experiment Boinc workloads requiring 4GB memory
* Same applies to workload duration, as preempting ongoing jobs of several hours will
  result in a significant loss of computing power - in some cases it might be
  worth to snapshot and restart later. On the other hand, a high
  graceful termination period might let the jobs finish but will impact the
  scheduler - here's a [nice overview](https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/#graceful-termination-of-preemption-victims) of this problem

And next? We are currently looking into additional optimizations of our Kubernetes setups
that should improve resource utilization beyond simply backfilling. We will
blog about them in the near future, and they include:

* Better packing of Pods in the cluster via scheduler optimizations, ensuring
  space is left for larger workloads arriving and helping the task of the
  cluster auto scaler. This is a not so well documented area, but [lots
  of flexibility available](https://github.com/kubernetes/kubernetes/blob/master/pkg/scheduler/algorithm/predicates/predicates.go#L53)
* Enable the cluster auto scaler in our clusters (running on OpenStack). The
  implementation is [already there](https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler/cloudprovider/magnum) and our bravest users have enabled it in
  their clusters

Have some spare capacity? Try our [helm chart](https://github.com/cernops/helm-boinc) and help physics research.
