---
title: 'Our Cloud in Liberty'
date: 2016-09-22T06:46:00.001-07:00
draft: false
author: Tim Bell
tags: ["openinfra"]
---

We have previously posted experiences with upgrades of OpenStack such as  

*   [Kilo](../our-cloud-in-kilo/)
*   [Juno](../our-cloud-in-juno/)
*   [Icehouse](../our-cloud-in-icehouse/)
*   [Havana](../our-cloud-in-havana/)

The upgrade to Liberty for the CERN cloud was completed at the end of August. Working with the upstream OpenStack, Puppet and RDO communities, this went pretty smoothly without any issues so there is no significant advice to report. We followed the same approach as the past, gradually upgrading component by component.  With the LHC reaching it's highest data rates so far this year (over 10PB recorded to tape during June), the upgrades needed to be done without disturbing the running VMs.

  

Some hypervisors are still on Scientific Linux CERN 6 (Kilo) using Python 2.7 in a software collection but the backwards compatibility has allows the rest of the cloud to migrate while we complete the migration of 5000 VMs from old hardware and SLC6 to new hardware on CentOS 7 in the next few months.  
  
After the migration, we did encounter a few problems:  

*   Live migration from Kilo to Liberty gave an error ([https://bugs.launchpad.net/nova/+bug/1576048](https://bugs.launchpad.net/nova/+bug/1576048))
*   Cannot delete machines with NUMA topology created before the upgrade ([https://bugs.launchpad.net/nova/+bug/1596119](https://bugs.launchpad.net/nova/+bug/1596119))
*   With the move to Nova API 2.1, a change in policy files was needed for our 'operator' role (details are at [delegation-of-roles](../delegation-of-roles/)). Some additional actions needed to be defined for the operator such as os_compute_api:servers:detail.

The first two cases are being backported to Liberty so others who have not upgraded may not see these.  
  
We've already started the Mitaka upgrade with Barbican, Magnum and Heat ahead of the other components as we enable a CERN production container service. The other components will follow in the next six months including the [experiences of the migration to Neutron](https://www.openstack.org/summit/barcelona-2016/summit-schedule/events/15865/neutron-at-cern-moving-thousands-of-production-nodes-from-nova-network) which we'll share at the OpenStack summit in October.

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

