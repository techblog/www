---
title: "A single cloud image for BIOS/UEFI boot modes on virtual and physical OpenStack instances"
date: 2020-05-17T15:00:00+02:00
draft: true
author: Arne Wiebalck
tags: ["openstack", "nova", "ironic", "bios", "uefi", "centos", "kickstart", "koji"]
---

“Brace yourselves: upcoming hardware deliveries may come with UEFI-only support.” This announcement from our hardware procurement colleagues a few months ago triggered the OpenStack and Linux teams to look into how to add UEFI support to our cloud images. Up to now, CERN cloud users had been using the *very same* image for virtual and physical instances and we wanted to keep it that way. This blog post summarises some of the tweaks needed to arrive with an image that can be used to instantiate virtual and physical machines, can boot both of these in BIOS and UEFI mode, and works with Ironic managed software RAID nodes for both BIOS/UEFI boot modes as well.

# TL;DR

We extended our CentOS-based image with

* a GPT type partition table (needed for UEFI and for disks >2TB)
* a BIOS boot partition (to compensate for the missing “MBR gap”)
* additional image properties (to boot a VM in UEFI mode)
* an EFI system partition (to store the EFI bootloader)
* the GRUB packages for EFI systems as well as the EFI boot manager (for cases where the bootloader needs to be relocated, like Software RAID setups)


In addition:

* For VMs, we installed the Open Virtual Machine Firmware (OVMF) from the EFI Development Kit (EDK2) on the hypervisors to have UEFI for x86 QEMU/KVM VMs.
* For physical machines, we used OpenStack Ironic on Stein with backported patches from Train and Ussuri for the Ironic Python Agent … easy!

# GPT support

Even before the need to support UEFI, the 2TB size limit for Master Boot Record (MBR) based partitions was raised as an issue by users deploying physical nodes via OpenStack Ironic with our default CentOS-based cloud image. While part of the [UEFI standard](https://uefi.org/specifications), GUID Partition Tables (GPT) do not require the node to boot in UEFI mode. Adding GPT support to a
BIOS image required merely to

* create a GPT partition table, and
* add a BIOS boot partition

An additional BIOS boot partition is necessary, since the usual “MBR gap”, i.e. the empty space right after the MBR left for file system alignment and usually used to store the second stage of bootloaders (such as [GNU GRUB](https://www.gnu.org/software/grub/)), does not exist in GPT disks (that space is here used to store actual partition table entries).

Since our image build process is to create a virtual machine (with [kickstart](https://pykickstart.readthedocs.io/en/latest/) and [koji](http://koji.build)) which is then snapshotted, we initially added something like

```bash
part biosboot --fstype=biosboot --size=1
...
/usr/sbin/parted --script /dev/vda mklabel gpt
```

to the kickstart file. Modulo a [bug in `growpart`](https://code.launchpad.net/~freddebacker/cloud-utils/cloud-utils/+merge/354305) we ran into, this allowed for larger disks -- and also prepared the ground to support UEFI.

# UEFI support for virtual instances and ...

In order to test new images in UEFI mode, our hypervisors needed to support booting UEFI based virtual machines in the first place. To allow libvirt to support virtual machines this required

* qemu 2.1+ and KVM on a kernel 3.10+
* chipset emulation pc-i440fx-2.1+
* the [OVMF UEFI firmware for QEMU and KVM](https://github.com/tianocore/tianocore.github.io/wiki/OVMF)

UEFI is supported when using the Q35 chipset emulation. By default we still use the i440FX emulation, however, so the image needed some additional parameters explicitly set:

```bash
hw_firmware_type='uefi'
hw_machine_type='q35'
architecture='x86_64'
```

Apart from an [issue in Nova with distribution support](https://review.opendev.org/#/c/348394/), this allowed us to boot a virtual machine in UEFI mode. For BIOS mode, the same image can be used, the `hw_firmware_type` needs to be cleared or explicitly set to (the default value) `bios`, though.

# … for physical instances

Physical instances in the CERN cloud are created via OpenStack Nova and Ironic. Since Ironic supports UEFI boot mode since the Juno release, there was nothing more to be done (except for setting the `boot_mode` in the nodes’ properties to `uefi` and to reconfigure the BIOS) in order to successfully boot physical instances in UEFI mode with the image we created for virtual machines.

# Prior art

Of course, we have not been the first ones to look into a BIOS/UEFI cloud image, so with some inspiration from the [upstream CentOS cloud image](https://github.com/CentOS/sig-cloud-instance-build/blob/master/cloudimg/CentOS-8-x86_64-Azure.ks), we enhanced the partitioning snippets in our kickstart file to:

```bash
bootloader --location=mbr --timeout=1 --append="console=ttyS0,115200 console=tty0" \
	--boot-drive=vda
part /boot/efi --onpart=vda15 --fstype=vfat --label=EFI
part / --fstype="xfs" --size=1 --grow --asprimary --label=ROOT

%pre
sgdisk --clear /dev/vda
sgdisk --new=14:2048:10239 /dev/vda # 4MiB
sgdisk --new=15:10240:128M /dev/vda # up to 128 MiB
sgdisk --typecode=14:EF02 /dev/vda # EF02: BIOS boot 
sgdisk --typecode=15:EF00 /dev/vda # EF00: EFI System
```

The sizes we picked for the partitions, 4MiB for the BIOS and 123MiB to fill things up to 128MiB, follow the upstream settings or fulfill the minimum requirements of 50MiB, respectively.

Since the final image will be a snapshot of a virtual machine, the bootloader configuration will reflect the boot mode of this instance. The source VM is in UEFI mode, so the BIOS bootloader needs to be installed as well:

```bash
grub2-mkconfig --output /etc/grub2-efi.cfg
grub2-install --target=i386-pc --directory=/usr/lib/grub/i386-pc/ /dev/vda
```

In addition, a BIOS grub2 config file needs to be derived from the UEFI one:

```bash
grub2-mkconfig --output=/boot/grub2/grub.cfg
EFI_ID=`blkid -s UUID -o value /dev/vda15`
BOOT_ID=`blkid -s UUID -o value /dev/vda1`
sed -i 's/gpt15/gpt1/' /boot/grub2/grub.cfg
sed -i "s/${EFI_ID}/${BOOT_ID}/" /boot/grub2/grub.cfg
sed -i 's|$prefix/grubenv|(hd0,gpt15)/efi/centos/grubenv|' /boot/grub2/grub.cfg
sed -i 's|load_env|load_env -f (hd0,gpt15)/efi/centos/grubenv|' /boot/grub2/grub.cfg
sed -i '/^### BEGIN \/etc\/grub.d\/30_uefi/,/^### END \/etc\/grub.d\/30_uefi/{/^### BEGIN \/etc\/grub.d\/30_uefi/!{/^### END \/etc\/grub.d\/30_uefi/!d}}' /boot/grub2/grub.cfg
sed -i -e 's|linuxefi|linux|' -e 's|initrdefi|initrd|' /boot/grub2/grub.cfg
```

Note that the required changes may differ between versions and that the snippet above is for CentOS7. All our configuration to build these images, i.e. also for CentOS8, can be found in the [koji image build repository](https://gitlab.cern.ch/linuxsupport/koji-image-build/) of the CERN Linux team.

The image generated with these changes allowed us to boot virtual machines and physical machines in both BIOS and UEFI mode.

# Support for physical machines with software RAID

A large fraction of the nodes in the CERN data centre use software RAID, the majority of these are created by Ironic, see also a previous post [here](https://techblog.web.cern.ch/techblog/post/ironic_software_raid/). In order to boot whole disk images on top of a software RAID, the bootloader needs to be relocated: it is stored inside a partition on top of a software RAID (made from partitions) and hence difficult to find for the BIOS otherwise. Ironic has added support for [software RAID with Train](https://docs.openstack.org/releasenotes/ironic/train.html) and [UEFI capable software RAID in Ussuri](https://docs.openstack.org/releasenotes/ironic/ussuri.html), so we backported the corresponding patch sets for initial support in [Train](https://review.opendev.org/#/q/topic:software_raid) as well as the ones for [Ironic](https://review.opendev.org/#/q/topic:install_bootloader_target_boot_mdoe) and the [Ironic Python Agent](https://review.opendev.org/#/q/topic:install_bootloader_changes) from Ussuri to our deployment based on Stein.

However, the need to relocate the bootloader infers another set of challenges which at least partially need to be addressed by the image: the environment from which the corresponding BIOS or UEFI tools for bootloader relocation are called needs to be in a state where these can actually successfully run. This requires some tools to be available (in the image as the relocation happens in a chroot’ed environment), like

* grub2-efi-x64 and grub2-efi-x64-modules
* efibootmgr and efivar-lib

but also that configuration files are in the expected places. The location of these files can differ between the boot modes, or they are even cross-linked. The grub config file, for instance, is expected to be in `/boot/grub2/grub.cfg` in BIOS mode, but in `/boot/efi/EFI/{distro}/grub.cfg` in UEFI mode. The grub environmental block (grubenv), is in `/boot/grub2/grubenv` for BIOS, but a symlink from `/boot/grub2/grubenv` to `/boot/efi/EFI/{distro}/grubenv` in UEFI mode. Hence, accessing all files may require the ESP partition to be mounted first. But even then the link for grubenv might get broken when `grub2-install` (for versions < 2.02-0.81) is installed on a machine booted in BIOS mode. It then needs to be restored with

```bash
$ rm -f /boot/grub2/grubenv
$ grub2-editenv list
```

We used a sufficiently recent version of grub2 and [added mounting the ESP to the Ironic RAID code](https://review.opendev.org/#/q/topic:story_2007618). This allowed us to finally boot a physical node with Ironic software RAID in BIOS and UEFI modes!

# Aftermath

Once all combinations were carefully tested once more, the new image was released to our users … and broke installations right away!

The reason was that the additional partitions in the new image came unexpected to cloud-init snippets where the partition layout was changed. Additional partitions may be desired to protect the root file system from a heavily over-committed file system such as `/tmp`!

Another issue we ran into was that the additional `grub.cfg` confused our Puppet configuration to adapt kernel parameters, e.g. for the L1TF mitigation or to configure huge pages. While recent versions of the `kernel_parameter` resource are aware that the `grub.cfg` file may be in different locations, it does not update all of them, but goes through an ordered list to pick the one to use (with preference for the UEFI location). As a consequence, the additional parameters make it to `/etc/default/grub`, but are used to only generate `/boot/efi/EFI/{distro}/grub.cfg` which is not the relevant file for an instance booted in BIOS mode.

These issues are meanwhile fixed/[raised upstream](https://github.com/traylenator/augeasproviders_grub/commit/4df5714fcc9292254bc6ba182b8cb77ece11d17f) … and helped us gaining more insight into what our users are doing with our carefully crafted images :) 

# Acknowledgments

Contributions to this work were made by Thomas Oulevey, Ben Morrice, Daniel Juarez, Alex Iribarren, Steve Traylen and Belmiro Moreira. Thanks!