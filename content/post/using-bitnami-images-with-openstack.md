---
title: 'Using bitnami images with OpenStack'
date: 2015-01-08T12:14:00.001-08:00
draft: false
author: Tim Bell
tags: ["openinfra"]
---

At CERN, we generally use [puppet](http://puppetlabs.com/) to configure our production services using modules from [puppetforge](https://forge.puppetlabs.com/) to quickly set up the appropriate parameters and services.  
  
However, it is often interesting to try out a new software package for a quick investigation. For this in the past, people have used Bitnami on test systems or their laptops where they installed the operating system and then installed the Bitnami application packages.  
  
With an OpenStack cloud, deploying [Bitnami](https://bitnami.com/) configurations can be even more quickly achieved. We are running OpenStack Icehouse and KVM or Hyper-V hypervisors.  
  
The steps are as follows  

*   Download the cloud image from Bitnami
*   Load the image into Glance
*   Deploy the image
*   Check the console for messages using Horizon
*   Use the application!

Since the operating system comes with the image, it also avoids issues with pre-requisites or unexpected configurations.

### Getting the images from Bitnami

Bitnami provides installers which can be run on operating systems that have been previously installed but also cloud images which include the appropriate operating systems in the virtual machine image.

  

There are a number of public clouds supported such as Amazon and Azure but also private cloud images for VMware and Virtual Box. For this example, we use the images for Virtual Box as there is a single image file.  
  
A wide variety of appliances are available. For this case, we show the use of [Wordpress](https://bitnami.com/stack/wordpress) (of course :-). The [Wordpress virtual machine private cloud list](https://bitnami.com/stack/wordpress/virtual-machine) gives download links. This contains the appliance image of Ubuntu, middleware and Wordpress in a zip file containing  
  

*   A OVF metadata file
*   A VMDK disk image

We only use the VMDK file.

### Loading the VMDK into Glance

A VMDK file is a disk image like a QEMU KVM qcow2 one. KVM also supports VMDK so it is possible to load it directly into Glance. Alternatively, it can be converted into qcow2 using qemu-img if this is needed.

```bash
glance image-create --name wordpress-bitnami-vmdk  --file bitnami-wordpress-4.1-0-ubuntu-14.04-OVF-disk1.vmdk --disk-format vmdk --container-format=bare
```

This creates the entry into Glance so that new VMs can be created.  

### Creating a new VM

The VM can then be instantiated from this image. 

```bash
nova boot --flavor m1.medium --image wordpress-bitnami-vmdk --key-name keypair hostname
```

The keyname and hostname text should be replaced by your preferred key pair and VM host name.


### Check console

Using the graphical interface, you can see the details of the deployment. Since ssh is not enabled by default, you can't log in. Once booted, you will see a screen such as below which gives instructions on how to access the application.

{{< figure src="../../img/post/bitnamiconsole.png"
     caption="Bitnami install console"
     class="caption"
>}}
  
If you wish to log in to the Linux shell, check the account details at the application page on Bitnami.

### Use the application

The application can be accessed using the web URL shown on the console above. This allows you to investigate the potential of the application by running a few simple OpenStack and web commands for a working application instance in a few minutes.

{{< figure src="../../img/post/bitnamiwordpress.png"
     caption="Bitnami Wordpress welcome"
     class="caption"
>}}  

**Note:** it is important that the application is kept up to date. Ensure you follow the Bitnami updates and ensure appropriate security of the server.

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

