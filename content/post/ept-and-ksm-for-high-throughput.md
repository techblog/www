---
title: 'EPT and KSM for High Throughput Computing'
date: 2015-08-02T10:35:00.002-07:00
draft: false
author: Tim Bell, Sean Crosby, Arne Wiebalck, Ulrich Schwickerath 
tags: ["openinfra"]
---

As part of the analysis of CERN's compute intensive workload in a virtualised infrastructure, we have been examining various settings of KVM to tune the performance.  
  

### EPT

[EPT](https://en.wikipedia.org/wiki/Second_Level_Address_Translation) is an Intel technology which provides hardware assist for virtualisation and is one of the options as part of the Intel KVM driver in Linux. This is turned on by default but can be controlled using the options on the KVM driver. The driver can then be reloaded as long as no qemu-kvm processes are running.  

```
# cat /etc/modprobe.d/kvm_intel.conf  
options kvm_intel ept=0  
# modprobe -r kvm_intel  
# modprobe kvm_intel  
```
  
In past [studies](https://gridka-school.scc.kit.edu/2011/downloads/CloudComputing_070911_Ulrich_Schwickerath.pdf), EPT has had a negative performance impact on High Energy Physics applications.  With recent changes in processor architecture, this was re-tested as follows.  

{{< figure src="../../img/post/eptksmeptsetting.png"
     caption="EPT KSM Comparison"
     class="caption"
>}}  

  
This is a 6% performance improvement with EPT off. This seems surprising as the functions are intended to improve virtualisation performance rather than reduce it.  
  
The CERN configuration uses hypervisors running CentOS 7 and guests running Scientific Linux CERN 6. With this configuration, EPT can be turned off without problems but a recent test with CentOS 7 guests has shown that this functionality has an issue which has been reported upstream. Only one CPU is recognised and the rest are reported as being unresponsive.  

### KSM

[Kernel same-page merging](https://en.wikipedia.org/wiki/Kernel_same-page_merging) is a technology which finds common memory pages inside a linux system and merges the pages so there is only a single copy, saving memory resources. In the event of one of the copies being updated, a new copy is created so the function is transparent to the processes on the system.

  

For hypervisors, this can be very beneficial where multiple guests are running with the same level of operating system. However, there is an overhead due to the scanning process which may cause the applications to run more slowly. 

  

We benchmarked 4 VMs, each 8 cores, running the same operating system levels. The results were that KSM causes an overhead of around 1%.

  

{{< figure src="../../img/post/eptksmksm.png"
     caption="KSM Comparison"
     class="caption"
>}}  
  
To turn KSM off, the ksmtuned daemon should be stopped.  

```
# systemctl disable ksmtuned  
```
  
The ksmd kernel thread still seems to run but does not use any CPU resources. Following the change, it is important to verify that there is still sufficient memory on the hypervisor since not merging the pages could cause an increase in memory usage and lead to swapping (which is a very significant performance impact)  
  
Previous blogs in this series are  

*   [CPU topology](../openstack-cpu-topology-for-high/)
*   [CPU model selection](../cpu-model-selection-for-high-throughput/)

### References

*   Intel article on EPT - https://01.org/blogs/tlcounts/2014/virtualization-advances-performance-efficiency-and-data-protection
*   Previous studies with KVM and HEP code - https://indico.cern.ch/event/35523/session/28/contribution/246/attachments/705127/968004/HEP\_Specific\_Benchmarks\_of\_Virtual\_Machines\_on\_multi-core\_CPU_Architectures.pdf
*   VMWare paper at https://www.vmware.com/pdf/Perf\_ESX\_Intel-EPT-eval.pdf

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

