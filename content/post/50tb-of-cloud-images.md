---
title: "50 TB of Cloud Images"
date: 2021-06-09T08:00:00+02:00
author: Belmiro Moreira
tags: ["openinfra", "openstack", "cern"]
---

Over the years, the number of Cloud images has been growing in the CERN OpenStack Cloud. We have accumulated more than 50 TB of available Cloud images/snapshots.
Unfortunately, Cloud operators have very few tools to control the growth of the OpenStack Image service.


## Glance Quotas

Glance is the OpenStack image service. It’s one of the oldest OpenStack projects (the 3rd oldest OpenStack project, released in Bexar) and looking into the last OpenStack survey (2020) it is the most used project in production environments (https://www.openstack.org/analytics/).

Different Cloud Infrastructures have different requirements regarding image upload and snapshot creation. At CERN, the Linux and Windows teams provide supported Cloud images for the general use cases. However, users can upload their own images to cover all the use cases for their specific work. Also, we encourage the creation of instance snapshots for all the applications that are manually configured or are critical for the Organization.

Images/snapshots are easy to create, but it turns out that the deletion process is much more “complicated”. Humans tend to forget to delete the resources that are not needed if they don’t have an incentive. In public Clouds the incentive is the cost of the resources. Private Clouds rely mostly on quotas!

The problem is that Glance doesn’t support quotas per tenant.

Different tenants have different requirements. These requirements can be vCPUs, RAM, Disk, … but also the amount of storage that should be allocated for images/snapshots.

Currently, Glance only supports global quotas. There are a few options related to image properties (“image_property_quota”), members (“image_member_quota”), tags (“image_tag_quota”) or location (“image_location_quota”), but I would like to focus on the following options:

```text
image_size_cap=SIZE
Optional. Default: 1099511627776 (1 TB)
Maximum image size, in bytes, which can be uploaded through the Glance API server.
```

```text
user_storage_quota=SIZE
Optional. Default: 0 (Unlimited).
This value specifies the maximum amount of storage that each user can use across all storage systems.
```

These options set the maximum size of an image and the maximum amount of storage per tenant, globally.

CERN Cloud runs a very heterogeneous set of applications in more than 4500 different tenants, that ranges from IT services to scientific data processing applications to users personal remote desktops. This means that we need to set the Glance global quota options considering the most demanding tenant, allowing all the other tenants to consume the exact same amount of storage resources for images and snapshots. As a result, these cap limits have very little effect.


## Available Images in the CERN Cloud

During the last 8 years we went from 0 to ~5000 available images/snapshots that used ~56 TB of storage space (end of March 2021). Actually, the storage resources needed are much higher because Ceph replicates the data and then we also back up all the images into S3 (a different Ceph cluster with replication) and we keep the deleted images for 30 days (in average ~2 TB).


{{< figure src="../../img/post/50tb-of-cloud-images-1.png"
     caption="Fig. 1 - Amount of storage used by the available images (per month and cumulative in GB)"
     class="caption"
>}}


We can see that the active images available in the CERN Cloud were created almost linearly over time and the amount of space added doesn’t differ much per month (excluding the 2 peaks during the last year explained by instance recreations between Nova cells).

Figure 2 shows the type of the images that users are creating and keeping.


{{< figure src="../../img/post/50tb-of-cloud-images-2.png"
     caption="Fig. 2 - Comparison between the storage space used by snapshots with all image types over the years"
     class="caption"
>}}


Looking into Fig. 2 we can conclude that most of the storage space is used by snapshots (actually ~90%) and some users have been keeping them since 2013!
Instance snapshots lose their value over time... however they are also very easy to “forget” without any incentive to delete them.

In our analysis we also detected several projects that still had VHD images/snapshots (the last HyperV compute nodes were removed at the beginning of 2017!).

The images/snapshots are owned by the projects, so any deletion decision must come from the users.


## Involving the Users to Delete Unnecessary Images/Snapshots

Seeing that storage space requirements will continue to grow because some “unnecessary” images/snapshots are not deleted by the users, we decided to contact all the responsibles from a small subset of projects (the projects with more “old” snapshots - taken before 2018). More than 350 project responsibles were contacted, reminding them about the “old” snapshots. We were impressed by the response… In a few weeks more than 20 TB of images/snapshots were deleted!


{{< figure src="../../img/post/50tb-of-cloud-images-3.png"
     caption="Fig. 3 - Storage space recovered when “old” snapshots were deleted"
     class="caption"
>}}


We could have continued the campaign and involved many more projects to recover more storage space. However, managing all this process is extremely challenging! Definitely, all of this would have been avoided if Glance had quotas per tenants.

PS: If you are wondering about the VHD images/snapshots, they were also deleted :)


## Moving forward

We have been discussing quotas per tenant with the Glance upstream community for several years. The Glance team always supported the idea of quotas per tenant, however task prioritisation in a small developer team and the aim to use the unified limits work always delayed the implementation of this feature. This problem was raised again in the last PTG (https://etherpad.opendev.org/p/xena-glance-ptg).

Thankfully, there has been a lot of work since then:
https://review.opendev.org/c/openstack/glance-specs/+/788037/2

I believe this proposal covers most of the issues related with Glance quotas. We really look forward to the Xena release to be able to deploy this feature.

Before ending, I would like to thank Dan Smith for picking up the work for the per tenant Glance quotas.
