---
title: "RadosGW Keystone Sync"
date: 2019-02-13T14:05:10+01:00
draft: false
author: Jose Castro Leon
tags : [radosgw, ceph, mistral, openstack]
---

We have recently enabled an S3 endpoint in CERN private cloud. This service is
offered by RadosGW on top of a Ceph cluster. This storage resource comes to
complement the cloud offering and allows our users to store object data using
S3 or swift APIs.

In order to enable the validation, you need to configure RadosGW to validate
the keys against the Identity service (Keystone) in OpenStack and then create
the new service and the endpoint in the Identity API.

## Here is the problem

Once we enabled the service, we started to see some performance impact while
validating the ec2 keys that are provided on our cloud. The key validation
follows this workflow.

{{< figure src="../../img/post/radosgw-sync-ec2-validation-workflow.png"
      caption="Ec2 key validation workflow"
      class="caption"
>}}

On every S3 operation, the key gets extracted and validated in Keystone. Then
this service needs to unpack it, verify its signature and then check if it
matches with any of the keys present on the system. This adds extra latency to
the key validation mechanism reducing the performance of the whole S3 service.

This performance hit has been firstly announced by our colleagues of SWITCH on
the Ceph Day in Berlin. You can access their slides on [Slideshare](https://www.slideshare.net/simonleinen/into-the-cold-object-storage-in-switchengines)

## Brainstorming

### Idea 1: Improve Keystone Credential validation

The first thing we have noticed is that the keys were stored in the database
and not cached in memory. Although we have added the necessary bits to cache
them in memory, this did not provide a noticeable improvement in the
performance. The majority of the time spent in the validation is on network
traffic and signature validation.

You can see the patch proposed to Keystone [bug1815771](https://review.openstack.org/#/c/636645/)

### Idea 2: What about caching the keys on RadosGW side?

If we look on the other side of the equation, RadosGW service have local
credential keys. Those keys are validated inside the RadosGW process, avoiding
the network dialogue with the Identity Service.

If we synchronize the keys accross the services, RadosGW will have a cached
copy of the keys per project. This sync job needs to be run frequently to
avoid issues with invalidated credentials. This method could complement the
existing credential check on keystone, in that model if the key is recent
then Keystone can validated until the sync puts it in the local credentials of
the account.

## Solution: Key synchronization Workflow

In the CERN private cloud, we have implemented both ideas to improve the
performance on the S3 service. We are running a synchronization workflow in
Mistral that synchronizes the keys every 15 minutes. This workflow does a
3-way synchronization between both services adding new keys present in
Keystone but not in RadosGW and removing keys in RadosGW that are not present
in Keystone.

On our Cloud, the S3 service is not available on all projects, it is available
on demand. Once it is enabled, the project will have a specific tag (s3quota).
This avoids to synchronize unnecessary keys.

This workflow relies in the libraries:

* [mistral-radosgw-actions](https://gitlab.cern.ch/cloud-infrastructure/mistral-radosgw-actions)
* [python-radosgw-admin](https://github.com/valerytschopp/python-radosgw-admin)

Every 15 minutes, the workflow "radosgw_key_sync.sync_job_main" will fetch all
all the credentials and the projects to sync, then it will filter out the
credentials that point to projects with no quota on S3. This workflow will
generate a sub-workflow per project to synchronize.

{{< figure src="../../img/post/radosgw-sync-global-workflow.png"
      caption="Global RadosGW Synchronization workflow"
      class="caption"
>}}

The sub-workflow 'sync_job_project' will fetch from RadosGW the keys
available. Then it will calculate the keys to be added and to be removed.
Finally it will do the API calls to add or remove them accordingly.

{{< figure src="../../img/post/radosgw-sync-project-workflow.png"
      caption="RadosGW Synchronization workflow per project"
      class="caption"
>}}

All the necessary code to use this sync is available [here](https://gitlab.cern.ch/cloud-infrastructure/mistral-workflows/blob/master/workbooks/radosgw_key_sync.yaml).

### Acknowledgements

This synchronization process could not be done without the help of

* CERN IT Storage team that provides an awesome Ceph service
* SWITCH team that implemented a radosgw python library and triggered the implementation workflow.

*Jose Castro Leon*
