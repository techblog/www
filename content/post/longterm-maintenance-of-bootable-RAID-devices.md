---
title: "Long-term Maintenance of Bootable Linux Software RAID Devices"
date: 2021-09-01T11:00:00+02:00
draft: false
author: Arne Wiebalck
tags: ["openinfra", "openstack", "ironic", "Software RAID", "md"]
---

Linux Software RAID (MD) devices are used at scale in the CERN IT data center. Primarily of course to protect from data loss due to a disk failure, but in more advanced setups we also use Software RAID devices also to protect the operating system’s boot disks themselves: a disk failure should never render a server unbootable. Protecting data via MD devices, but also having redundant boot devices, has been a well-established setup for many years, decades by now probably. While this all works fine at setup time, and even during the dry-run tests right after, things get a little more hairy when you need to maintain such systems over several years. A disk failure on a RAIDed boot disk may be detected and the faulty disk replaced by a spare one, but what needs to be done to make sure that we move to a protected state again? Just add the disk and sync the RAIDs, you say? Well (spoiler alert!)... no :) In this post we look into some of the issues we ran into and the steps we had to add in our setups to make sure a RAIDed boot disk setup maintains the protection you expect.

## Structuring the diversity

With a total fleet size of more than 10’000 bare metal servers we deal with multiple hardware generations and their corresponding configurations and interfaces: BIOS vs. UEFI boot mode, IPMI vs Redfish BMC protocol, HDD vs. SSD vs. NVMe drives … to name some relevant in the context of this article. To make things even more fun, the services relying directly on bare metal servers, rather than virtual ones, use different ways to install the servers (and configure the MD devices): OpenStack Ironic vs. Kickstart based installation vs Windows specific solutions ... all this makes a nice zoo of combinations. The relevant points for managing bootable Linux Software RAID devices during the years of a server’s lifetime, however, are the boot mode and the installation mechanism, so we will focus on the four cases of 

- BIOS vs. UEFI bootmode, and
- Ironic vs Kickstart/Anaconda based installations

 We’ve had cases for all four setups where we needed to adapt things after installation and even **after** nodes were moved into production. While this may sound scary at first, it was not a real problem in reality: the issues we will discuss in the following have either been found without a real incident (after all CERN is a physics lab and we like Gedankenexperiments[^1]) or on nodes which were taken out of production for some other reason. As usual, the mitigations were carefully tested and then rolled out.[^2]

[^1]: The German term for [thought experiment](https://en.wikipedia.org/wiki/Einstein%27s_thought_experiments).
[^2]: End of management-targeted disclaimer.

## The (relatively) straight-forward one: UEFI

Since the beginning of 2020, CERN IT is taking a proactive approach of using UEFI for all new server deliveries, mainly in order to facilitate easier firmware upgrades. But this decision also affected the way servers are being installed. The Ceph and the OpenStack teams in CERN IT are examples for services which rely on different mechanisms to install hypervisors and OSD servers, respectively: while the cloud team uses [OpenStack Ironic](ironicbaremetal.org) (basically pre-built images are streamed onto the root disk), the Ceph team relies on [Anaconda and Kickstart](https://fedoraproject.org/wiki/Anaconda/Kickstart) (an installer follows instruction how to configure the system and which packages to install). The reasons for these choices are mostly that the cloud team offers Ironic as a service (think dogs and food), while the disk layout requirements for Ceph OSDs are currently beyond what can easily expressed via OpenStack.[^3]

Trying to make sure disk failures do not prevent a machine from booting, the Ceph team was first to look into what this requires. Since they rely on Anaconda, they amended their kickstart manifest with something along the lines of:

```bash
ignoredisk --only-use=sda,sdb
clearpart --all --initlabel --drives sda,sdb

# for /boot
partition raid.01 --size 1024 --ondisk sda
partition raid.02 --size 1024 --ondisk sdb

# for /boot/efi
partition raid.11 --size 256  --ondisk sda
partition raid.12 --size 256  --ondisk sdb

# for /
partition raid.21 --size 1    --ondisk sda --grow
partition raid.22 --size 1    --ondisk sdb --grow

raid /boot     --level=1 --device=boot     --fstype=xfs raid.01 raid.02
raid /boot/efi --level=1 --device=boot_efi --fstype=efi raid.11 raid.12
raid /         --level=1 --device=root     --fstype=xfs raid.21 raid.22
```

This creates a RAIDed EFI (Extensible Firmware Interface) system partition (ESP) in addition to the ones for `/boot` and `/` (note the `--fstype=efi`). During boot, the UEFI firmware will look for an ESP partition signature and access it to find the bootloader. By RAIDing it, any update to the bootloader will go to both disks (as the updates come from the OS and the OS will mount the RAID device).

Note that the RAID level is explicitly set to version 1. This is to make sure the RAID metadata is at the end of the disk. This way, the beginning of the disks looks the same as if the disks were not in a RAID at all, and the UEFI firmware does not need to understand the ESP is RAIDed.[^4] In fact, the RAID device is not assembled at this stage anyway, but the firmware can get at the bootloader. Seems all pretty straightforward. The only caveat is the fact we allow the UEFI firmware to access one leg of a RAID device, we bypass the MD interface. This should not be a problem as we only read … *checks UEFI specification* … oops: the UEFI specification explicitly allows the firmware to also write to an ESP![^5] Writing to one leg will break the MD device. It is not clear if UEFI firmware implementations actually do this and we have not observed this with our servers, but in order to be safe, you will need to check (and potentially) repair the MD device every time you assemble it.[^6]

On to the cloud team and their Ironic managed MD devices. The support for UEFI Software RAID in Ironic was introduced in the Ussuri release[^7], and by the time the Ceph team started looking into RAIDed ESPs the OpenStack team still considered the Ironic implementation (creating ESPs on all disks) as safe: the kernels to boot are not on the ESP, only the bootloaders. This was also “confirmed” by downgrading/updating kernels. We overlooked, however, that the grub configuration is also on the ESP. So, if you update enough kernels, the configuration on the disk used by the UEFI firmware may reference kernels which have been removed from the system (and leave us with a system which can only boot from one disk). This was fixed in Ironic upstream by creating RAIDed ESPs[^8], but still left us with quite some nodes with potentially vulnerable servers in production. We fixed these live (😱)! The performed steps included:

- some sanity checks
-- is this a UEFI node?
-- are the ESPs already mirrored?
-- do we have an EFI entry in `/etc/fstab`?
- creating a backup of the current ESP content
- refreshing the configuration file from the current system: - `grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg`
- making a copy of the just created ESP content as well
- unmounting and wiping the device used for `/boot/efi`
- creating an MD mirror from all EPS devices (with `--metadata=1.0`, see above)
- creating a filesystem with a known UUID on the new md mirror
- replacing the UUID for the ESP in `/etc/fstab` and mount it
- moving the EPS content back
- setting the default kernel to the latest installed kernel (not the latest kernel!)

Looks more scary than it is ... or maybe only in hindsight ... anyway, this seemed to work fine and we did this on all affected nodes.

At this stage, and with some support by motivated reasoning (so not our fault, really), we convinced ourselves all is good: the UEFI nodes were fixed and on BIOS nodes the boot loader is on mirrored storage by construction (on `/boot`). Only the few bytes of the very static pre-bootloader stages in the MBR and the MBR gap at the beginning of the disk are not mirrored, but this should not be an issue. As you might guess, we were wrong ...


[^3]: This may change with the current work on the Anaconda driver in Ironic.
[^4]: This [blog post](https://outflux.net/blog/archives/2018/04/19/uefi-booting-and-raid1/) gives some more details on this.
[^5]: [UEFI Specification version 2.9, March 2021](https://uefi.org/sites/default/files/resources/UEFI_Spec_2_9_2021_03_18.pdf) (Ch. 13: Protocols - Media Access, p. 500ff.).
[^6]: The Ceph service does this at boot time and uses the usual MD mechanism to repair a device which reports errors. There is also a simulator to mimic how a kernel update (or any other write) will change the ESP and whether it can be successfully repaired: [EFI Checker](https://github.com/dvanders/efi).
[^7]: See also a previous blog post with all details on [Ironic Software RAID](https://techblog.web.cern.ch/techblog/post/ironic_software_raid/).
[^8]: [Ironic Story on RAIDed ESPs](https://storyboard.openstack.org/#!/story/2008745).

## The (surprisingly) nasty one: BIOS

We were finally woken up from our Dornröschenschlaf[^9] by a node which refused to reboot shortly after a disk was replaced. So, apparently we did not move back to a sane state after the repair. The instructions to perform after the physical replacement of a disk included:

- replicate the partition table from another healthy disk
- run `grub2-install` to install the bootloader on the new device
- do a hexdump comparison of the first 440 bytes on all disks (not the whole 512 bytes of the first sector/MBR since it contains an ID which is unique for the physical disk)

But these steps were clearly not sufficient: they all checked out, but the server did not boot. Or, more precisely: it did not boot from any but the newly added device (this specific node had four disks mirrored, so we had more than just one other disk to try).

The first suspect was the grub multi-staging: the bootloader has only a small binary (`boot.img`) in the MBR (stage1) which points to a binary (`core.img`) in the MBR gap -- the space between the MBR and the first partition -- (stage1.5) which points to `/boot` (stage2).

So, we should probably compare `core.img` as well and if it is different, re-install grub on that disk … 

```bash
# with an MBR/dos partition table, core.img is in the MBR gap
MD5_COREIMG=`dd if=${BOOTDISKS[0]} bs=512 skip=1 count=63 2>/dev/null|md5sum`
echo "COREIMG MD5UM on ${BOOTDISKS[0]}: ${MD5_COREIMG}"
for i in "${BOOTDISKS[@]:1}"
do
    MD5=`dd if=${i} bs=512 skip=1 count=63 2>/dev/null|md5sum`
    echo "COREIMG MD5UM on ${i}: ${MD5}"
    if [ ! "${MD5}" = "${MD5_COREIMG}" ]
    then
        echo "ERROR: core.img not the same on all disks"
        exit 1
    fi
done
```

This indeed fixed the issue! An explanation for this is that grub does not only install `boot.img` and `core.img`, but also the bootloader in `/boot`, which is shared in an MD setup. When installing grub on the new disk, we started to mix these three components from potentially different releases of the grub package. While this is fine for the new disk, the older disks had therefore an old `boot.img` and `core.img`, but with a new `/boot`! So, the main message from this post is probably:

**When you insert a new device into a RAIDed set of boot disks, (re-)install the bootloader on all member disks, not only the new one!**

Equipped with this knowledge we went on to scan all our BIOS servers for mismatches and re-installed the bootloader when necessary ... until we ran into a case where this did not work!

The check script verified the contents of `boot.img` and `core.img` after bootloader installation, but in some cases the contents were not matching after grub had been re-installed on all disks. This is when we learnt about the ‘BIOS Parameter Block’ from another blog post.[^10] Grub does in fact not write the whole MBR, it preserves some space which may be used by specific file systems. The very first bytes on the disk indicate the length of this block: in our case `eb 63 90` which translates to `jump 0x63 noop`, so the bytes between `0x02` and (`0x01` + `0x63`) are left untouched. If there is something there, grub will not change it. This explains why the checksums during the comparison did not match. We adapted the check to read the jump information and only compared the first 440 bytes minus this block. To get and compare the initial jump distance on all disks, we used something like:

```bash
# retrieve and compare the jump over the parameter block
JUMP=`dd if=${BOOTDISKS[0]} bs=1 count=3 2>/dev/null|hexdump -C|head -n1|cut -d ' ' -f2-5`
echo "Initial jump on ${BOOTDISKS[0]}:${JUMP}"
for i in "${BOOTDISKS[@]:1}"
do
    JUMP_TMP=`dd if=${i} bs=1 count=3 2>/dev/null|hexdump -C|head -n1|cut -d ' ' -f2-5`
    echo "Initial jump on ${i}:${JUMP_TMP}"
    if [ ! "${JUMP}" = "${JUMP_TMP}" ]
    then
        echo "ERROR: initial jump not the same on all disks"
        exit 1
    fi
done
JUMP_DISTANCE_HEX=`echo ${JUMP}|cut -d ' ' -f2`
JUMP_DISTANCE_DEC=`echo "ibase=16; ${JUMP_DISTANCE_HEX}" | bc`
```

So, another issue fixed and we resumed our scanning … until we ran into a case where even this was not enough! 

We missed that not all our BIOS nodes have an MBR partition table, some have a one in GPT format. In this case, `core.img` is not in the MBR gap (which does not exist), but inside the boot partition. We adapted the scanner to detect this, find the boot partition, and compare `core.img` from there. With this new version, we continued our scanning ... and found no more surprises :)

We consider all our servers ok now (haven’t we been here before?) and think that they will continue to boot no matter which disk has failed and how long the mirrored boot disks are in operation.

## Postface

Glad to see you made it until here! Interest in bootloader structures, MD device intricacies, or BIOS vs UEFI subtleties is usually only sparked when something is broken. So, let’s hope this post helps with understanding the underlying issue if this is the case for you or, if not, reduces the need to look into these details ever again :)

Thanks to the CERN Ceph team (Dan van der Ster and Enrico Bocchi) for the discussions and help on this quest ... and mostly for triggering it!

[^9]: The author aplogizes for the second German term, explained in [Dornröschenschlaf](https://en.wiktionary.org/wiki/Dornr%C3%B6schenschlaf).
[^10]: [Linux MBR blog post](http://www.sharetechnote.com/html/Linux_MBR.html).