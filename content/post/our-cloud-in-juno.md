---
title: 'Our cloud in Juno'
date: 2015-05-12T07:38:00.006-07:00
draft: false
author: Tim Bell
tags: ["openinfra"]
---

This blog continues our series around upgrades of OpenStack. Previous upgrades are documented in  

*   [Our cloud in Havana](https://techblog.web.cern.ch/techblog/post/our-cloud-in-havana/)
*   [Our cloud in Icehouse](https://techblog.web.cern.ch/techblog/post/our-cloud-in-icehouse/)

At CERN, we do incremental upgrades of the cloud, component by component. Giving details of the problems we encounter along the way.  
  
For Juno, we followed the pattern as previously  

*   cinder
*   glance
*   keystone
*   ceilometer
*   nova
*   horizon

As we are now rolling out our CentOS 7 based controllers, we took the opportunity to do that upgrade also. Many of the controllers themselves are virtualised which allows us to scale out as needed. An HA proxy configuration allows us to switch rapidly between the services on the different levels.

The motivation to move to CentOS 7 comes from two primary sources

*   CERN is moving from its own distribution, Scientific Linux CERN, to CentOS 7.
*   The RDO packages are being produced on CentOS 7 now. This means that we can benefit from community testing if we are also on that version.

We'll give more details on the SLC6 environment in a future posting.

We encountered one problem during the upgrade. The LDAP backend for roles compared the definition with an upper case role definition. We were using lower case roles in LDAP. This was resolved with a quick workaround and a bug will be reported around [https://github.com/openstack/keystone/blob/stable/juno/keystone/assignment/backends/ldap.py#L93](https://github.com/openstack/keystone/blob/stable/juno/keystone/assignment/backends/ldap.py#L93).  
  
Other than that, the upgrade proceeded smoothly and we're now looking forward to deploying Heat and starting the planning for the migration to Kilo.

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

