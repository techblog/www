---
title: 'Swiss and Rhone Alpes User Group Meeting at CERN'
date: 2013-12-09T04:02:00.004-08:00
draft: false
author: Tim Bell
tags: ["openinfra"]
---

  
A combined meeting of the Swiss OpenStack and Rhone Alpes OpenStack user groups was held at CERN on Friday 6th December 2013. This is the 6th [Swiss User Group](http://www.meetup.com/openstack-ch/) meeting and the 2nd [Rhone Alpes one](http://www.meetup.com/OpenStack-Rhone-Alpes/).  
  
Despite the cold and a number of long distance travelers, 85 people from banking, telecommunications, Academia/Research and IT companies gather to share OpenStack experiences across the region and get the latest news.  
  
The day was split into two parts. In the morning, there was the opportunity for a visit to the ATLAS experiment, see a 3-D movie on how the experiment was built and visit the CERN public exhibition points at the Globe and Microcosm. Since the Large Hadron Collider is currently under going maintenance until 2015, the experimental areas are accessible for small groups with a guide.  
  
At the ATLAS control room, you could see a model of the detector  
  
[![IMG_0330](http://farm6.staticflickr.com/5509/11241247834_d1781a5d2d.jpg)](http://www.flickr.com/photos/110870439@N08/11241247834/ "IMG_0330 by moreirabelmiro, on Flickr")  
  
The real thing is slightly bigger and heavier... 100m underground, only one end is accessible currently from the viewing gallery.  
  
{{< figure src="../../img/post/atlascern.jpg"
caption="ATLAS"
class="caption"
>}}
  
The afternoon session presentations are available at the [conference page](https://indico.cern.ch/conferenceTimeTable.py?confId=273593#20131206).  

{{< figure src="../../img/post/swissrhonecrowd.JPG"
caption="Swiss Rhone Alps Crowd"
class="caption"
>}}  

After a quick introduction, I gave [feedback on the latest board meeting](https://indico.cern.ch/getFile.py/access?contribId=0&resId=1&materialId=slides&confId=273593) in Hong Kong with topics such as the election process and the [defcore](http://robhirschfeld.com/2013/11/22/defcore/) discussion to answer the "What is OpenStack" question.

The following talk was an set of technical summaries of the Hong Kong summit from Belmiro, Patrick and Gergely. Belmiro covered the [latest news on Nova, Glance and Cinder](https://indico.cern.ch/getFile.py/access?contribId=2&resId=5&materialId=slides&confId=273593) along with some slides from his [deep dive talk on CERN's openstack cloud](http://www.openstack.org/summit/openstack-summit-hong-kong-2013/session-videos/presentation/deep-dive-into-the-cern-cloud-infrastructure).

  
[![IMG_0368](http://farm8.staticflickr.com/7367/11241021715_d0e1ab29f3.jpg)](http://www.flickr.com/photos/110870439@N08/11241021715/ "IMG_0368 by moreirabelmiro, on Flickr")  
  
Patrick from Bull's [xlcloud](http://www.xlcloud.org/bin/view/Main/) covered the latest news on Heat which is rapidly becoming a key part of the OpenStack environment as it not only provides a user facing service for orchestration but also is now a pre-requisite for other projects such as Trove, the database as a service.  
  
[![IMG_0376](http://farm3.staticflickr.com/2863/11240984455_e96c2132c6.jpg)](http://www.flickr.com/photos/110870439@N08/11240984455/ "IMG_0376 by moreirabelmiro, on Flickr")  
  
In a good illustration of the difficulties of compatibility, the open office document failed to display the key slide on PowerPoint but Patrick covered the details while the PDF version was brought up. Heat currently supports AWS Cloud Formations but is now adding a native template language, HOT, to cover additional functions. The icehouse release will add more auto scaling features, integration with ceilometer and a move towards the [TOSCA](https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=tosca) standard.  
  
Gregely covered some of the user stories and the latest news on ceilometer as it starts to move into alarming on top of the existing metering function.  
  
[![IMG_0379](http://farm3.staticflickr.com/2829/11241061913_8889044c21.jpg)](http://www.flickr.com/photos/110870439@N08/11241061913/ "IMG_0379 by moreirabelmiro, on Flickr")  
  
Alessandro then covered [the online clouds at CERN](https://indico.cern.ch/getFile.py/access?contribId=1&resId=1&materialId=slides&confId=273593) which are opportunistically using the 1000s of servers attached to the CMS and ATLAS experiments when the LHC is not running.  The aim is to be able to switch as fast as possible from the farm being used to filter the 1PB/s from the LHC to performing physics work. Current tests show it takes around 45 minutes to intantiate the VMs on the 1,400 hypervisors.  
  
[![IMG_0386](http://farm8.staticflickr.com/7289/11240936736_2120e44dbb.jpg)](http://www.flickr.com/photos/110870439@N08/11240936736/ "IMG_0386 by moreirabelmiro, on Flickr")  
  
Jens-Christian gave a talk on the use of [Ceph at SWITCH](https://indico.cern.ch/getFile.py/access?contribId=4&resId=1&materialId=slides&confId=273593). Many of the aspects seem similar to the block storage that we are looking at within CERN's cloud. SWITCH are aiming a 1,000 core cluster to serve the Swiss academic community including dropbox, IaaS and app-store style services. It was particularly encouraging to see that SWITCH have been able to perform online upgrades between versions without problems.... the regular warning to be cautious with CephFS is also made, so using rbd rather than filesystem backed storage makes sense.  
  
[![IMG_0392](http://farm4.staticflickr.com/3693/11240885406_1b1d3d3278.jpg)](http://www.flickr.com/photos/110870439@N08/11240885406/ "IMG_0392 by moreirabelmiro, on Flickr")  
  
Martin gave us a detailed view on the options for geographically distributed clouds using OpenStack. This was intriguing on multiple levels in view of CERN's ongoing work with the community on federated identity along with some useful hints and tips on the different kinds of approaches. Martin converged onto using Regions to achieve the ultimate goals but there were several potential useful intermediate configurations such as cells which CERN is using extensively in the multi-data centre cloud with Budapest and Meyrin. I fully agree with Martin's perspective on the need for cells to become more than just a nova concept as we require similar functions in Glance and Cinder for the CERN use case. Martin had given the same talk in Paris on Thursday and was giving it again on Monday in Israel so he is doing a fine job in re-use of presentations.  
  
[![IMG_0394](http://farm8.staticflickr.com/7429/11240869934_66a8d6c2f0.jpg)](http://www.flickr.com/photos/110870439@N08/11240869934/ "IMG_0394 by moreirabelmiro, on Flickr")  
  
Sergio described Elastic Cluster which is an [open source tool](https://github.com/gc3-uzh-ch/elasticluster) for provisioning clusters for researchers. He illustrated the function with a [youtube video](http://www.youtube.com/watch?v=cR3C7XCSMmA&feature=youtu.be) which demonstrates the scaling of the cluster on top of a cloud infrastructure.  
  
[![IMG_0399](http://farm6.staticflickr.com/5483/11240781005_6b2bf92803.jpg)](http://www.flickr.com/photos/110870439@N08/11240781005/ "IMG_0399 by moreirabelmiro, on Flickr")  
  
Finally, Dave Neary gave an [introduction to Open Shift](https://indico.cern.ch/getFile.py/access?contribId=7&resId=1&materialId=slides&confId=273593) and how to deploy a Platform-as-a-Service solution. Using a simple git/ssh model, various PaaS instances can be deployed easily on top of an OpenStack cloud. The talk included a memorable demo where Dave showed the depth of his ruby skills but was rescued by the members of the audience and the application deployed to rounds of applause.  
  
[![IMG_0407](http://farm6.staticflickr.com/5526/11240735204_b296a3ce0c.jpg)](http://www.flickr.com/photos/110870439@N08/11240735204/ "IMG_0407 by moreirabelmiro, on Flickr")  
  
Many thanks to the CERN guides and administration for helping with the organisation, all of the attendees for coming and making it such a lively meeting and to Sven Michels and [Belmiro Morerira](http://www.flickr.com/photos/110870439@N08/sets/72157638419127263/) for the photos.

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

