---
title: 'os_type property for Windows images on KVM '
date: 2017-02-09T01:29:00.002-08:00
draft: false
author: Jose Castro Leon
tags: ["openinfra", "glance", "windows"]
---

  
The OpenStack images have a long list of properties which can set to describe the image meta data. The full list is described in the [documentation](http://docs.openstack.org/cli-reference/glance-property-keys.html). This blog reviews some of these settings for Windows guests running on KVM, in particular for Windows 7 and Windows 2008R2.  
  
At CERN, we've used a number of these properties to help users filter images such as the OS distribution and version but also added some additional properties for specific purposes such as  
  

*   when the image was released (so the images can be sorted by date)
*   whether the image is the latest recommended one (such as setting the CentOS 7.2 image to not recommended when CentOS 7.3 comes out)
*   which CERN support team provided the image 
  
For a typical Windows image, we have  
  
```
$ glance image-show 9e194003-4608-4fe3-b073-00bd2a774a57
+-------------------+----------------------------------------------------------------+
| Property          | Value                                                          |
+-------------------+----------------------------------------------------------------+
| architecture      | x86_64                                                         |
| checksum          | 27f9cf3e1c7342671a7a0978f5ff288d                               |
| container_format  | bare                                                           |
| created_at        | 2017-01-27T16:08:46Z                                           |
| direct_url        | rbd://b4f463a0-c671-43a8-bd36-e40ab8d233d2/images/9e194003-4   |
| disk_format       | raw                                                            |
| hypervisor_type   | qemu                                                           |
| id                | 9e194003-4608-4fe3-b073-00bd2a774a57                           |
| min_disk          | 40                                                             |
| min_ram           | 0                                                              |
| name              | Windows 10 - With Apps [2017-01-27]                            |
| os                | WINDOWS                                                        |
| os_distro         | Windows                                                        |
| os_distro_major   | w10entx64                                                      |
| os_edition        | DESKTOP                                                        |
| os_version        | UNKNOWN                                                        |
| owner             | 7380e730-d36c-44dc-aa87-a2522ac5345d                           |
| protected         | False                                                          |
| recommended       | true                                                           |
| release_date      | 2017-01-27                                                     |
| size              | 37580963840                                                    |
| status            | active                                                         |
| tags              | []                                                             |
| updated_at        | 2017-01-30T13:56:48Z                                           |
| upstream_provider | https://cern.service-now.com/service-portal/function.do?name   |
| virtual_size      | None                                                           |
| visibility        | public                                                         |  
+-------------------+----------------------------------------------------------------+
```
  

Recently, we have seen some cases of Windows guests becoming unavailable with the [BSOD](https://en.wikipedia.org/wiki/Blue_Screen_of_Death) error "CLOCK_WATCHDOG_TIMEOUT (101)".  On further investigation, these tended to occur around times of heavy load on the hypervisors such as another guest doing CPU intensive work.  
  
Windows 7 and Windows Server 2008 R2 were the guest OSes where these problems were observed. Later OS levels did not seem to show the same problem.  
  
We followed the standard processes to make sure the drivers were all updated but the problem still occurred.  
  
Looking into the root cause, the [Red Hat support articles](https://access.redhat.com/solutions/755943) were a significant help.  
  
"In the environment described above, it is possible that 'CLOCK\_WATCHDOG\_TIMEOUT (101)' BSOD errors could be due to high load within the guest itself. With virtual guests, tasks may take more time that expected on a physical host. If Windows guests are aware that they are running on top of a Microsoft Hyper-V host, additional measures are taken to ensure that the guest takes this into account, reducing the likelihood of the guest producing a BSOD due to time-outs being triggered."  
  
These suggested to use the os_type parameter to help inform the hypervisor to use some additional flags. However, the OpenStack [documentation](http://docs.openstack.org/cli-reference/glance-property-keys.html) explained this was a XenAPI only setting (which would not therefore apply for KVM hypervisors).  
  
It is not always clear which parameters to set for an OpenStack image. Setting os_distro has a value such as 'windows' or 'ubuntu'. While the flavor of the OS could be determined, the setting of os_type is needed to be used by the code.  
  
Thus, in order to get the best behaviour for Windows guests, from our experience, we would recommend setting both the os_distro and os_type as follows.  

*   os_distro = 'windows'
*   os_type = 'windows'

When the os_type parameter is set, some additional XML is added to the KVM configuration following the [Kilo](https://bugs.launchpad.net/nova/+bug/1400315) enhancement.

  
```
<features>
    <acpi/>
    <apic/>
    <hyperv>
      <relaxed state='on'/>
      <vapic state='on'/>
      <spinlocks state='on' retries='8191'/>
    </hyperv>
  </features>

  ....

  <clock offset='localtime'>
    <timer name='pit' tickpolicy='delay'/>
    <timer name='rtc' tickpolicy='catchup'/>
    <timer name='hpet' present='no'/>
    <timer name='hypervclock' present='yes'/>
  </clock>
```


These changes have led to an improvement when running on a loaded hypervisors, especially for Windows 7 and 2008R2 guests. A [bug](https://bugs.launchpad.net/openstack-manuals/+bug/1662991) has been opened for the documentation to explain the setting is not Xen only.

References
----------

*   Property keys documentation for OpenStack at [http://docs.openstack.org/cli-reference/glance-property-keys.html](http://docs.openstack.org/cli-reference/glance-property-keys.html)
*   Additional functionality added to Nova to add hyper-v timer enlightenments for windows guests at [https://bugs.launchpad.net/nova/+bug/1400315](https://bugs.launchpad.net/nova/+bug/1400315)
*   Documentation bug report at [https://bugs.launchpad.net/openstack-manuals/+bug/1662991](https://bugs.launchpad.net/openstack-manuals/+bug/1662991)
*   Red Hat articles at [https://access.redhat.com/solutions/755943](https://access.redhat.com/solutions/755943) and [https://access.redhat.com/node/1471613](https://access.redhat.com/node/1471613)

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

