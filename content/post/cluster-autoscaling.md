---
title: "Cluster Autoscaling for Magnum Kubernetes Clusters"
date: 2019-05-22T12:00:00+02:00
draft: true
author: Thomas Hartland
tags: ["kubernetes", "openstack", "magnum", "containers"]
---

The [Kubernetes cluster autoscaler](https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler)
has been in development since 2016, with early support for the major public cloud providers for Kubernetes.
But, there has been no way to use it running Kubernetes on OpenStack until now,
with the addition of the [autoscaler cloud provider for Magnum](https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler/cloudprovider/magnum).

As an OpenStack cloud operator with around 400 Magnum clusters (majority Kubernetes),
CERN has a lot to gain from the flexibility that the cluster autoscaler provides.

With the increasing adoption of container technologies both for powering the services we provide
and by the users of our cloud, efficiently managing the computing resources used by our container clusters becomes more important.

## The how and why

The core logic of the cluster autoscaler is simple to explain, but is backed up by full simulations of the Kubernetes scheduling process.
In short, pods that are stuck in the pending status because there are not enough CPU or memory resources to schedule them in the cluster
trigger the autoscaler to scale up, and nodes which remain unused by any pod for a set period of time are considered for removal by scaling down.

Though simple, the criteria for scaling up and down are highly configurable.
For example, the responsiveness of the autoscaler can be altered through several delay parameters, so that it could wait before scaling up,
or change the delay before unused nodes are eligible for removal.
Pod priority can be taken into consideration to decide whether a given pod should cause a scale up at all, and allows
the autoscaler to freely delete pods below a priority threshold when scaling down. This pairs extremely well
with the horizontal pod autoscaler, for backfilling a cluster with low priority pods.
We wrote about this in a [previous post](https://techblog.web.cern.ch/techblog/post/priority-preemption-boinc-backfill/).

The autoscaler will also try to move pods around if there are nodes with low utilisation (<50% by default).
This ensures that close to the minimum number of nodes are used, but of course there are some pods which you
would not want to be disrupted in any situation. To prevent this, the pods can be given the following annotation:

```
cluster-autoscaler.kubernetes.io/safe-to-evict: "false"
```

To manually prevent scaling down a particular node, there is a node annotation for that as well:

```
cluster-autoscaler.kubernetes.io/scale-down-disabled: "true"
```

Using kube-scheduler priorities which prefer to place pods on the nodes which already have the highest utilisation
also helps to efficiently pack pods into the smallest possible number of nodes. This will be the topic of another post.

For services which occasionally receive very large bursts of load, one way to ensure
that the service is able to continue functioning normally during those bursts is to
overprovision the number of VM instances running that service.
However this leads to underutilisation of CPU and memory resources for extended periods
of time between the bursts. Here is a particularly bad case of overprovisioning:

{{< figure src="../../img/post/cluster-autoscaling-service-cpu-use.png"
     caption="An example of a bad case of overprovisioning"
     class="caption"
>}}

By moving services to Kubernetes and dynamically matching the required number of pods/nodes to the amount of load,
by combining the horizontal pod autoscaler and the cluster autoscaler,
the overall resource usage would be greatly improved.

Once the cluster autoscaler has gained the confidence of our users,
we plan to enable autoscaling by default in all clusters.
Until then, it will remain as an opt-in parameter at cluster creation.

## An example

The most basic example for the cluster autoscaler is to start in a single node cluster,
create some workload that requires more nodes, and then to remove the workload and
scale back down to the initial node only.

```
$ openstack coe cluster list
+--------------------------------------+--------------+------------+--------------+-----------------+
| uuid                                 | name         | node_count | master_count | status          |
+--------------------------------------+--------------+------------+--------------+-----------------+
| 3d843295-dc44-4c63-8f0b-3da60238064c | cluster      |          1 |            1 | CREATE_COMPLETE |
+--------------------------------------+--------------+------------+--------------+-----------------+
```

With the cluster ready to use, and the autoscaler deployed either automatically or manually, it is time
to create some load.

For a testing workload you can use pods which do nothing, but request a whole CPU or more each.
In this example only a single pod is able to be scheduled on the one available node.

```
$ kubectl create -f largepods.yaml 
deployment.apps/largepods-deployment created

$ kubectl get pods
NAME                                    READY   STATUS    RESTARTS   AGE
largepods-deployment-748c47fcd8-2d8g9   0/1     Pending   0          10s
largepods-deployment-748c47fcd8-bqhhr   0/1     Pending   0          10s
largepods-deployment-748c47fcd8-dgl77   1/1     Running   0          10s
largepods-deployment-748c47fcd8-kzg5w   0/1     Pending   0          10s
largepods-deployment-748c47fcd8-txt7t   0/1     Pending   0          10s
```

To be able to run the four pending pods, the cluster autoscaler triggers a scale up,
adding four more nodes.

```
$ kubectl -n kube-system logs cluster-autoscaler-55589d8bd4-fjn5r
I0430 13:43:27       1 scale_up.go:689] Scale-up: setting group DefaultNodeGroup size to 5
I0430 13:43:28       1 magnum_nodegroup.go:101] Increasing size by 4, 1->5
I0430 13:44:01       1 magnum_nodegroup.go:67] Waited for cluster UPDATE_IN_PROGRESS status
I0430 13:47:37       1 magnum_nodegroup.go:67] Waited for cluster UPDATE_COMPLETE status
```

As you can see in the logs, the scale up from start to finish takes a little over 4 minutes.
The time that the scale up takes is dependent on factors like whether the VM image is already present on
the hypervisors that the nodes are created on. When each node has finished spawning it registers itself 
with the Kubernetes master node and is ready for scheduling pods on, so one slow node will not stall the entire scaling process.

```
$ kubectl get pods
NAME                                    READY   STATUS    RESTARTS   AGE
largepods-deployment-748c47fcd8-2d8g9   1/1     Running   0          4m50s
largepods-deployment-748c47fcd8-bqhhr   1/1     Running   0          4m50s
largepods-deployment-748c47fcd8-dgl77   1/1     Running   0          4m50s
largepods-deployment-748c47fcd8-kzg5w   1/1     Running   0          4m50s
largepods-deployment-748c47fcd8-txt7t   1/1     Running   0          4m50s

$ openstack coe cluster list
+--------------------------------------+--------------+------------+--------------+-----------------+
| uuid                                 | name         | node_count | master_count | status          |
+--------------------------------------+--------------+------------+--------------+-----------------+
| 3d843295-dc44-4c63-8f0b-3da60238064c | cluster      |          5 |            1 | UPDATE_COMPLETE |
+--------------------------------------+--------------+------------+--------------+-----------------+
```

When the workload is removed, the cluster autoscaler will remove the four nodes it added.

```
$ kubectl delete -f largepods.yaml 
deployment.apps "largepods-deployment" deleted

$ kubectl -n kube-system logs cluster-autoscaler-55589d8bd4-fjn5r
I0430 13:53:18       1 scale_down.go:882] Scale-down: removing empty node cluster-n2aefv63qv27-minion-1
I0430 13:53:18       1 scale_down.go:882] Scale-down: removing empty node cluster-n2aefv63qv27-minion-2
I0430 13:53:18       1 scale_down.go:882] Scale-down: removing empty node cluster-n2aefv63qv27-minion-3
I0430 13:53:18       1 scale_down.go:882] Scale-down: removing empty node cluster-n2aefv63qv27-minion-4
I0430 13:53:24       1 magnum_manager_heat.go:279] Waited for stack UPDATE_IN_PROGRESS status
I0430 13:54:25       1 magnum_manager_heat.go:279] Waited for stack UPDATE_COMPLETE status
I0430 13:54:58       1 magnum_nodegroup.go:67] Waited for cluster UPDATE_IN_PROGRESS status
I0430 13:55:30       1 magnum_nodegroup.go:67] Waited for cluster UPDATE_COMPLETE status

$ openstack coe cluster list
+--------------------------------------+--------------+------------+--------------+-----------------+
| uuid                                 | name         | node_count | master_count | status          |
+--------------------------------------+--------------+------------+--------------+-----------------+
| 3d843295-dc44-4c63-8f0b-3da60238064c | cluster      |          1 |            1 | UPDATE_COMPLETE |
+--------------------------------------+--------------+------------+--------------+-----------------+
```

If the cluster also has Prometheus/Grafana deployed in it,
this can be monitored with a Grafana plot that shows these two metrics

* `sum(kube_node_status_allocatable_cpu_cores{node!~".*master.*"})`
* `sum(kube_pod_container_resource_requests_cpu_cores{node!~".*master.*"})`

{{< figure src="../../img/post/cluster-autoscaling-example.png"
     caption="Grafana plot of requested and available CPU resources during the above example."
     class="caption"
>}}

The cluster autoscaler also exposes its own metrics, which show more detailed information
about the number of nodes that have been added and removed, number of unschedulable pods, 
number of unneeded nodes, and more.

## DIY

When deploying the cluster autoscaler, there are some things you should pay particular attention to.

Currently there is no concept of a node group in Magnum, so the cluster autoscaler will treat the entire set of minions as a single node group.
The size restrictions on the node group are set by the parameter `--nodes=1:10:DefaultNodeGroup` in the deployment.
The `1:10` are the minimum and maximum constraints. The maximum should be set to a value that prevents the autoscaler from overflowing
the quota of the OpenStack project that the cluster is in, as the cluster autoscaler is not aware of the quota limits.

Since only one node group is possible, setting more than one node group in the
`--nodes`  parameter is not supported yet.
Node groups will be coming to Magnum with the Train release.

There is a list of parameters and descriptions on the cluster autoscaler
[FAQ page](https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/FAQ.md),
and these should be tuned to suit your production workload requirements.
Or for quick testing the cluster autoscaler can be run with these parameters:

```
--scale-down-unneeded-time=1m
--scale-down-delay-after-add=1m
--scale-down-delay-after-failure=1m
```

## What's next?

Development of the Magnum cloud provider for the cluster autoscaler is finished for now,
but there are several new and upcoming features in Magnum which will be incorporated
into a second iteration of the cloud provider.

The cluster resize API, released in Magnum Stein, simplifies the scaling process by giving
Magnum control over which nodes nodes are removed when scaling down the cluster. Currently this
requires interacting with Heat, which makes the process slower and more complex.

When node groups are added in Magnum Train it will bring the autoscaler cloud provider for Magnum in line with
the features that other cloud providers offer. It will enable independently scaled node groups which can differ
in machine type, size, availability zone, and provide different hardware (i.e nodes with GPUs).

At CERN we will be trying to use autoscaling wherever it is feasible, and already have several use cases planned.
These will be documented and made into their own blog post, so keep an eye out for that.
