---
title: Kubernetes, CSI and Storage at CERN
subtitle: What we have now, what we're looking at next
date: 2021-03-05T00:00:00+02:00
author: Ricardo Rocha
tags: ["kubernetes", "storage", "csi", "cosi", "cephfs", "manila", "eos", "cvmfs"]
---

Many moons ago we had a series of posts covering container storage, CSI and
CephFS. Recently we've been reviewing our storage integration with Kubernetes
and deciding where to focus next. This post comes out of that.

The diagram below presents a (very) simplified overview of how Kubernetes
exposes APIs to the clients and the different integration points for tools in
different areas.

{{< figure src="../../img/post/kubernetes-storage-api.png"
    caption="" width="100%" >}}

The core API includes familiar things like Deployments or Services, and with [Custom
Resource Definitions
(CRD)](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)
we can benefit from the built-in scheduler and
controller manager to handle pretty much any other kind of resource - virtual
machines with KubeVirt, databases, among thousands others. I'm sure someone
somewhere is controlling their light bulbs with `kubectl apply` but does not dare to say it.

On the backend side there are multiple interfaces abstracting the container
runtime, networking, storage and handling the interactions with cloud
providers for things like load balancing or cluster auto scaling.

### Kubernetes Storage at CERN

Storage requirements for a workload can be expressed as Persistent Volume
Claims (PVC) which have some information regarding the type of storage desired -
fast, slow, a specific backend, etc. A matching provisioner will then
either give back an existing volume or dynamically provision one, in a process transparent to the client.

{{< figure src="../../img/post/kubernetes-storage-pvc-storageclass.png"
    caption="" width="100%" >}}

Previous to CSI support volume drivers were built-in the main Kubernetes tree,
and the only way to plug custom storage was using the FlexVolume driver. This
had several issues and things improved significantly with CSI even if it can be
a bit complex at start.

{{< figure src="../../img/post/kubernetes-storage-backends.png"
    caption="" width="100%" >}}

At CERN we have multiple storage systems, each serving different purposes. The
integrations are done differently for each of them as summarized in the picture
above.

CEPH is used for block storage, shared filesystems and object storage. We rely on the
upstream [CSI drivers](https://github.com/ceph/ceph-csi) and the [Cinder](https://github.com/kubernetes/cloud-provider-openstack/blob/master/docs/cinder-csi-plugin/using-cinder-csi-plugin.md) and [Manila](https://github.com/kubernetes/cloud-provider-openstack/blob/master/docs/manila-csi-plugin/using-manila-csi-plugin.md) integrations for the first two, with ways to express QoS for block
storage, and to choose specific backend clusters for shared filesystems.

The CernVM filesystem (CVMFS) is implemented as a FUSE module and offers
a scalable read-only filesystem with local caches (squid like), and is widely used for
software distribution in our distributed grid environment. We implemented
a [custom CSI driver](https://github.com/cernops/cvmfs-csi) in this case, which handles the required mounts for the
different repositories.

[EOS](https://eos-web.web.cern.ch/eos-web/) is the main backend for our physics data and hosts 100s of PBs of data.
It offers a hierarchical filesystem and is again accessible with
a FUSE module. We expose it with
a [DaemonSet](https://gitlab.cern.ch/helm/charts/cern/-/tree/master/eosxd) loading the required modules and
daemons, and users need to add a hostPath to their deployments.

### Issues and Next Steps

We already have hundreds of clusters relying on the setup above, but still miss some
functionality to support all our use cases.

#### Snapshots in CephFS

Volume snapshots are available for a while in Kubernetes, and even supported by the
upstream CephFS CSI driver. In the same way a PVC is a request for a volume to
be provisioned, a VolumeSnapshot tells the system to perform a snapshot of
a volume. The same goes for the VolumeSnapshotterClass and its StorageClass
equivalent.

{{< figure src="../../img/post/kubernetes-storage-snapshots.png"
    caption="" width="100%" >}}

But we have not yet enabled this in our deployments, mostly due to
the functionality in CephFS still being somewhat unreliable. This is something that our
users would greatly benefit from, it can be hard to listen to reports of
custom tooling performing manual copies to external storage.

We have two main goals here, described in the figure above.

The first is to give users a
way to easily snapshot (and restore) their volumes - mostly so they can cover for
issues with their own applications, the reliability of CEPH being a good
guarantee for the rest already. The second is to offer a DR solution by
doing backups of the snapshots to an external location.

In both cases we're
considering [Velero](https://velero.io) as it has integration with CSI and
should support any other backend we might add in the future as well. The other
benefit we get from Velero is that it will handle the restore step for CephFS
snapshots - there is no built-in mechanism in CephFS to fill a new volume from an
existing snapshot, the content has to be copied in.

#### One Volume to Rule Them All

This is a common request from our CVMFS users: in many cases applications serve
a wide range of communities and it is hard (or impossible) to know in advance
all the CVMFS repositories that should be made available. Applications need to
express each repository they need access to as a PVC, which can be a problem.

{{< figure src="../../img/post/kubernetes-storage-cvmfs-automount.png"
    caption="" width="100%" >}}

The request usually comes in the form:
> Could i have a single PVC for the whole /cvmfs, and automount each repository 
> on the first access?

This is how our virtual machine based setups handle this problem, but it is not
clear how this can be done while relying on CSI. Maybe the solution is to move
out of CSI in this case and somehow achieve this with a DaemonSet setup
instead, but we'll investigate the solution drawn above as well.

#### OAuth2 Token Based Access

One of the challenges we have accessing storage (and other services) is
how to handle kerberos credentials in a containerized environment. How users can
make their personal credentials available, how these can be renewed for long running
jobs, and how to make them available to the daemons handling the remote mounts.

{{< figure src="../../img/post/kubernetes-storage-oauth-sidecar.png"
    caption="" width="100%" >}}

With the popularity of notebooks the challenge is renewed but now for OAuth2
tokens, since most user facing web applications have integration with the CERN
SSO using OIDC - some of our storage systems already take an OAuth2 token
instead of a Kerberos credential. Work here has been to come up with a sidecar
that will handle token renewal for an application/pod in a transparent way.

#### Object Storage APIs

The missing bit in our original picture above is how to handle object storage.

{{< figure src="../../img/post/kubernetes-storage-api-cosi.png"
    caption="" width="100%" >}}

This is handled by the Container Object Storage Interface (COSI), which fills
the gap where the CSI is not a good fit since it focuses on block and
filesystem storage. We're excited
to start integrating this into our deployments and offer users a way to access
object storage (on-premises and external) using the Kubernetes APIs.

#### Keep Pushing Forward

We often describe many of our workloads as *embarassingly parallel* as
they do not require any sort of communication between the different steps -
which means we can very easily parallelize to thousands or tens of
thousands of individual steps.

{{< figure src="../../img/post/kubernetes-storage-scaleout.png"
    caption="" width="100%" >}}

In more recent use cases we can have ML workloads involving a distributed model
training or hyperparameter optimization with hundreds or thousands of steps, where some sort of shared storage
is required. This poses challenges in the throughput required and the number of
concurrent clients to our storage systems. The next months will include several
scale tests to see where our limits are, and if in some cases we should rely on
in-cluster storage instead of centralized systems to reduce the noise on other
(potentially critical) services.

We will continue improving our infrastructure working in close collaboration with the Kubernetes and
CNCF communities and projects. If you have thoughts and ideas regarding the
problems we describe above let us know, we would love to discuss them further.
