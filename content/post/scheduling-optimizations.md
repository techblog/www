---
title: "OpenStack Nova Scheduling Optimizations at CERN"
date: 2019-07-05T08:30:00+02:00
author: Belmiro Moreira
tags: ["openstack"]
---

CERN Cloud Infrastructure has been available since 2013. From the beginning we started to use Nova Cells. Initially only with 2 Cells the Infrastructure has been growing to more than 70 Cells. Cells is a Nova functionality that enables operators to shard the Infrastructure. Sharding the Infrastructure has innumerous advantages in large deployments. For example:

- Avoid the message broker and DB bottleneck
- Isolate failure domains and consequently improve availability
- Transparent to the end user

In 2018 we upgraded from what is now known as CellsV1 (Ocata) to CellsV2 (Queens).

It was a really huge operations challenge and a large milestone for our Cloud Infrastructure. More details about this upgrade were given in the OpenStack Summit Vancouver - 2018 - [Moving from CellsV1 to CellsV2 at CERN](https://www.youtube.com/watch?v=49CFXNIDM3c)

CellsV2 tries to achieve the same goals as CellsV1 however the implementation/design is completely different.

One of the significant differences that we experience is how the scheduling is performed. With CellsV1 the "nova-scheduler" runs per Cell. In contrast with CellsV2 scheduling is global. As operational result the scheduling time increased from few milliseconds to several seconds.

{{< figure src="../../img/post/scheduling-optimizations-1.png"
     caption="Fig. 1 - Scheduling time for production instances after upgrading to CellsV2"
     class="caption"
>}}

Since then we have been working to improve the scheduling time of instances when using multiple Cells.

In order to improve the scheduler performance we reduced the maximum number of allocation candidates that placement will retrieve to the scheduler changing the configuration option [scheduler.max_placement_results = 10](https://docs.openstack.org/nova/latest/configuration/config.html#scheduler.max_placement_results)

The scheduler time was almost reduced by half to an average of ~7 seconds. 

However, changing this configuration option had significant side effects. Placement doesn’t know about disabled compute nodes that are retrieved as allocation candidates and only then removed by the Scheduler. As a result some instances would fail with the infamous "No Valid Host" error message. This issue is being addressed by the following specification [pre-filter-disabled-computes](https://specs.openstack.org/openstack/nova-specs/specs/train/approved/pre-filter-disabled-computes.html). Another consequence was when trying to force the allocation of an instance to a particular host in operations like cold/live migrations. If the host was not in the 10 allocation candidates retrieved by Placement the operation would fail. This will be finally be fixed in Train release with [#649535](https://review.opendev.org/#/c/649535/). Until now the mitigation has been simply removing the limit for this particular operations like, for example, in [#576693](https://review.opendev.org/#/c/576693/).

We also noticed that the scheduler would need to go through all the Cells to pull out the services/compute nodes info. This code was parallelized with [#539617](https://review.opendev.org/#/c/539617/) however contacting all Cells was taking a significant time. Considering that we map projects to Cells using Placement request-filter [#544585](https://review.opendev.org/#/c/544585/) and each availability zone is mapped to few Cells, we decided to only contact the Cells from which we get allocation candidates from Placement. To achieve this we use the information available in host mappings. See: [#635532](https://review.opendev.org/#/c/635532/).

This approach has a significant impact in the scheduling time in our Cloud Infrastructure when Placement is configured to return only few allocation candidates. The scheduling time was reduced by half, to an average of ~4 seconds. However, we still think that these values can be reduced even more.

{{< figure src="../../img/post/scheduling-optimizations-2.png"
     caption="Fig. 2 - Scheduling time when #635532 was applied"
     class="caption"
>}}

The Nova team started to investigate the scheduler performance to identify possible bottlenecks. One of the issues that was identified by Jay Pipes was that when using [filter_scheduler.track_instance_changes = False](https://docs.openstack.org/nova/latest/configuration/config.html#filter_scheduler.track_instance_changes) the performance of the HostManager can
be negatively impacted. He proposed the following solution to mitigate the problem: [#623558](https://review.opendev.org/#/c/623558/).

This work optimizes the number of queries that “HostManager._get_host_states()” does in order to get the information about the each host retrieved by placement. This essentially changes the algorithm's performance from O(N) to O(1).

We tested this work (referred as [#623558](https://review.opendev.org/#/c/623558/)) and compared it with our host_mappings patch (referred as [cern]( https://review.opendev.org/#/c/635532/), stable/stein (referred as “upstream”), and different [scheduler.max_placement_results](https://docs.openstack.org/nova/latest/configuration/config.html#scheduler.max_placement_results) (10 and the default 1000).

For the test we patched 3 of our production nova-schedulers with [#623558](https://review.opendev.org/#/c/623558/), [#623558](https://review.opendev.org/#/c/623558/) + [cern]( https://review.opendev.org/#/c/635532/), [cern]( https://review.opendev.org/#/c/635532/), respectively, plus another one running the upstream code (stable/rocky).

The results of Fig. 3 and Fig. 4 represent the time that the different 4 nova-schedulers took to schedule user instances in our production infrastructure.

{{< figure src="../../img/post/scheduling-optimizations-3.png"
     caption="Fig. 3 - Average time to schedule instances considering the different patches using 'max_placement_results=10'"
     class="caption"
>}}

{{< figure src="../../img/post/scheduling-optimizations-4.png"
     caption="Fig. 4 - Average time to schedule instances considering the different patches using 'max_placement_results=1000'"
     class="caption"
>}}

Using [scheduler.max_placement_results=10](https://docs.openstack.org/nova/latest/configuration/config.html#scheduler.max_placement_results) we can see that the combination of "[cern]( https://review.opendev.org/#/c/635532/)”, “[cern]( https://review.opendev.org/#/c/635532/) + [#623558](https://review.opendev.org/#/c/623558/)” patches produces the best results in our Infrastructure. However, running only [#623558](https://review.opendev.org/#/c/623558/) doesn't has a significative impact.

When increasing [scheduler.max_placement_results=1000](https://docs.openstack.org/nova/latest/configuration/config.html#scheduler.max_placement_results) the "cern" patch produces very bad results. The overhead to calculate the location of all the allocation candidates is very expensive. However, [#623558](https://review.opendev.org/#/c/623558/) produces very good results when comparing with “upstream”.

As conclusion "cern" patch produces very good results if the number of allocation candidates is small. The [#623558](https://review.opendev.org/#/c/623558/) patch improves a lot stable/stein when the number of allocation candidates is high.

After this results we decided to not apply [#623558](https://review.opendev.org/#/c/623558/) until is merged upstream and continue to use a small number of allocation candidates.

Confused after all these numbers? Let's try to wrap up some conclusions:

- Scheduling in our Infrastructure takes more time than what we think it should;
- Several parallelization issues where found and already fixed upstream.
- Those in addition with the patch [#635532](https://review.opendev.org/#/c/635532/) can have significant positive impact in the scheduler performance when having a small number of allocation candidates.
- The patch [#623558](https://review.opendev.org/#/c/623558/) performs very well when dealing with a big number of allocation candidates. 

We continue to investigate how can we improve even more the scheduling time. Recently new patches where proposed, like [#663388](https://review.opendev.org/#/c/663388). We will continue to evaluate the impact of these changes in our Infrastructure.

For now, we went from ~20 seconds one year ago to ~4 seconds today!

Many thanks to Surya Seetharaman and all OpenStack Nova team for all the great work.

