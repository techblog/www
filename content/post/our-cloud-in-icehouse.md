---
title: 'Our cloud in Icehouse'
date: 2014-11-06T14:58:00.000-08:00
draft: false
author: Tim Bell
tags: ["openinfra"]
---

This is going to be a very simple blog to write.  
  
When we upgraded the CERN private cloud to Havana, we wrote a [blog post](https://techblog.web.cern.ch/techblog/post/our-cloud-in-havana/) giving some of the details of the approach taken and the problems we encountered.  

The high level approach we took this time was the same, component by component. The first ones were upgraded during August and Nova/Horizon last of all in October.

The difference this time is that no significant problems were encountered during the production upgrade.  
  
The most time consuming upgrade was Nova. As last time, we took a very conservative approach and disabled the API access to the cloud during the upgrade. With offline backups and the database migration steps taking several hours given the 1000s of VMs and hypervisors, the API unavailability was around six hours in total. All VMs continued to run during this period without issues.

Additional Functions
--------------------

With the basic functional upgrade, we are delivering the following additional functions to the CERN end users. These are all based off the OpenStack Icehouse release functions and we'll try to provide more details on these areas in future blogs.

*   [Kerberos and X.509 authentication](https://techblog.web.cern.ch/techblog/post/kerberos-and-single-sign-on-with/)
*   Single Sign On login using CERN's Active Directory/ADFS service and the SAML federation functions from Icehouse
*   Unified openstack client for consistent command lines across the components
*   Windows console access with RDP
*   IPv6 support for VMs
*   Horizon new look and feel based on RDO styling
*   Delegated rights to the operators and system administrators to perform certain limited activites on VMs such as reboot and console viewing so they can provide out of working hours support.

Credits
-------

Along with the CERN team, many thanks should go to the OpenStack community for delivering a smooth upgrade across Ceilometer, Cinder, Glance, Keystone, Nova and Horizon.

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

