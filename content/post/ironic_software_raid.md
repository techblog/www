---
title: "Software RAID support in OpenStack Ironic"
date: 2019-09-09T12:15:09+02:00
draft: false
author: Arne Wiebalck
tags: ["openstack"]
---

The vast majority of the ~15’000 physical servers in the CERN IT data centers rely on Software RAID
to protect services from disk failures. With the advent of OpenStack Ironic to manage this bare
metal fleet, the CERN cloud team started to work with the upstream community on adding Software RAID
support to Ironic’s feature list. Software RAID support in Ironic is now ready to be released with
OpenStack’s Train release, but (backported to Stein) the code is already in production on more than
1’000 nodes at CERN. This blog post will explain what is needed to deploy a bare metal instance with
a Software RAID configuration, but also what Ironic is doing under the hood to set up a node with
Software RAID.

# Preparing and configuring an Ironic node with Software RAID

**Note:** *The exhaustive documentation with all the details on how to set up (Software) RAID devices
with Ironic is available from the [Ironic Admin Guide](https://docs.openstack.org/ironic/latest/admin/raid.html).
In this post, we will limit ourselves to a high level view and a basic example.*

From an operator’s perspective, the configuration of a Software RAID follows the configuration of a
hardware RAID and consists of three basic steps:

1. set the Ironic node’s RAID interface (`raid_interface`)
1. define the Ironic node’s desired RAID configuration (`target_raid_config`)
1. apply the RAID setup via manual cleaning

The `raid_interface` of a node defines which driver is used to configure a RAID. In the case of Software
RAID this should be set to `agent`:

```bash
$ openstack baremetal node set --raid-interface agent <NODE_UUID_OR_NAME>
```

As we will see later, in contrast with (most) hardware RAIDs, a Software RAID in Ironic is configured
in-band, that is by the Ironic Python Agent (IPA) from inside the agent RAM disk. As the support for Software
RAID is part of the agent’s generic hardware manager, there is no need to bundle a specific hardware
manager with the agent’s RAM disk as is generally required for hardware RAID setups, though.

The second step on the way to deploy an instance on an Ironic managed software RAID is to specify the
desired Software RAID configuration via the node’s `target_raid_config` property. The `target_raid_config`
is a dictionary of `logical_disks` (i.e. RAID devices) to be created, where each logical disk is in turn
a dictionary containing its properties. For a minimal configuration, an operator needs to define at least
one logical disk, its RAID level, its size (in this specific case `MAX` denotes to use all available space
on the underlying disks), and that it is a Software RAID device (done by setting `"controller": "software"`):

```JSON
{
  "logical_disks": [	
    {
      "raid_level": "1",
      "size_gb"   : "MAX",
      "controller": "software"
    }
  ]
}
```

Via a file (or directly from a string) this needs to be passed to Ironic to be attributed to a node:

```bash
$ openstack baremetal node set <NODE_UUID_OR_NAME> --target-raid-config <JSON_FILE_WITH_TARGET_RAID_CONFIG>
```

The third and final step is to apply this configuration via
[manual cleaning](https://docs.openstack.org/ironic/latest/admin/cleaning.html#manual-cleaning). In general,
the cleaning steps to create or delete a RAID are called `create_configuration` and `delete_configuration`, and
these are also used when setting up a Software RAID. As required when defining cleaning steps, the operator
needs to define the interface and the desired step in a JSON format:

```JSON
[{
  "interface": "raid",
  "step": "delete_configuration"	
 },
 {
  "interface": "deploy",
  "step": "erase_devices_metadata"	
 },
 {
  "interface": "raid",
  "step": "create_configuration"	
 }]
```

When creating Software RAID devices, it is recommended to explicitly delete previously set up devices and remove
any metadata on the disk as the RAID creation may fail otherwise (a precaution to not destroy existing devices
accidentally!). The above config needs to be stored to a file, then the actual cleaning (and hence the creation
of the RAID device) is triggered by:

```bash
$ openstack baremetal node clean <NODE_UUID_OR_NAME> --clean-steps <JSON_FILE_WITH_CLEAN_STEPS>
```

Once the node is back from manual cleaning, the node’s disks will have a Software RAID configured. The node can
now be provided and, once available, is ready for instance deployment!

## How Ironic sets up an instance on a Software RAID node

As indicated in the previous section, the implementation of Software RAID support in Ironic and the IPA leveraged the already
existing framework to set up hardware RAIDs. This framework provides the ability to define
the desired RAID configurations via the `target_raid_config` to automatically verify the validity of passed configurations,
the communication between the Ironic conductor and the IPA to execute cleaning steps, and the functionality to update a
node’s `raid_config` whenever a RAID is created or deleted.

The main interface to this framework are the `{create,delete}_configuration` functions of the `GenericHardwareManager` class.
These functions are supposed to be implemented by custom hardware managers to define the respective RAID functionality.
In order to support Software RAID, these functions have now been implemented in the `GenericHardwareManager` itself to
configure Linux Software RAID devices.


Triggered by manual cleaning for a node in state `manageable`, the creation of partitions on the holder disks is the first
real step in the Software RAID creation process by the `GenericHardwareManager`. Why does Ironic partition the disks? Mostly,
in order to ease potential issues with the boot process, in particular with understanding and assembling RAID devices during
the early boot stages. To help with this, Ironic limits the level of the first RAID device to RAID-1. This increases the
chances that booting works as the RAID-1 members are basically indistinguishable from stand-alone disks. This RAID-1 device
by default is then also the device the IPA will pick for deployment. In order to provide different RAID levels for the
“payload” devices, the holder disks are partitioned and all RAID devices are created on top of these partitions. 

The assembly of the RAID device(s) via `mdadm` according to the layout requested in the `target_raid_config` is the second step
in the process, still executed by the `GenericHardwareManager` in the IPA as part of the manual cleaning. Upon successful
completion, the node finishes cleaning, moves back to state `manageable` and starts waiting for provisioning and deployment.
The `raid_config` of the node has been automatically updated by Ironic’s RAID post-processing hooks. The operator has hence
access to the information which RAID layout a node has and when it was applied.

Upon instance deployment, Ironic creates partitions for the root device and the configuration drive, completely unaware that
the device it uses is in fact a Software RAID device on top of partitions on top of real disks. The final stack of devices
and partitions after these operations looks like this:

￼{{< figure src="../../img/post/ironic-software-raid-fig1.png"
     caption="Fig. 1 - Device stack on an Ironic Sofware RAID"
     class="caption"
>}}

In a remaining step, the conductor triggers the IPA to perform the installation of the bootloader. For whole disk images, this
step is usually not required since the boot loader is an integral part of the deployed image. For partition images and for the
deployment on top of Software RAIDs, however, the bootloader needs to be explicitly installed by the IPA. In order to find the
correct location for the bootloader, the IPA needs to follow the chain from the root device (`/dev/md0p1`) to the underlying Software
RAID device (`/dev/md0`), find the corresponding component devices (`/dev/sd{a,b}{1,2}`) before it can identify all holder disks
(`/dev/sda{a,b}`) onto which it needs to install the bootloader. Afterwards, the instances will boot from a Software RAID-1 with
an additional “payload” device which can be mounted via a configuration management system or via `cloud-init` (which is what we do
here at CERN).

The cleaning of Software RAID devices includes the removal of all created md devices as well as the created partitions, and also
resets the node’s `raid_config` property.

## Current limitations of Software RAID and future work

As often the case for initial implementations, there are certain limitations for the Software RAID support in Ironic. In addition to
the ones that we consciously introduced to keep the first version simple (like limiting the support to RAID levels 0, 1, and 10), or
a maximum number of two RAID devices which can be defined in total per node, we’d like to highlight three areas where further efforts
could be directed.

In its current version, Ironic’s software RAID support does not provide means to select the disks which shall become holder disks for
a software RAID, (following Ironic’s paradigm to “take over the world!”) it simply takes them all! While this might be ok for 2-disk
servers with RAID-0 or RAID-1, or even 4-disk servers with RAID-{0,1,10}, this may be an issue for servers with more disks or servers
with disk arrays. The `logical_disks` definition via the `target_raid_config` needs to be extended beyond considering only the mandatory
properties.

One typical aim when configuring Software RAID devices is to protect from disk failures and handle them transparently for the application.
For RAID-0, however, single disk failures break the Software RAID device and leave it in an unusable state (in fact, upon reboot, the
kernel reports a broken RAID-0 as an inactive device with a spare, so that `lsblk` changes the device’s type -- a fact which becomes relevant
for md device detection and subsequent cleaning). There is no support in Ironic to recover from such a situation, apart from the (admittedly
very cloudy) approach to delete the instance, delete/create a (new) RAID device, and re-create the instance eventually. Despite the similarity
in the name, Nova’s `rebuild` does not help here (as it expects a healthy device for deployment), but it would be nice if the delete/create
Software RAID device steps could be built into rebuilding instances via Nova.

A third issue to address, also related to the previous one, is the fact that Software RAID support in Ironic is admin rather than user driven:
the definition of the `logical_disks` requires admin privileges, and so does triggering the application of the configuration via manual cleaning.
To mitigate this to some degree, the CERN Ironic deployment uses a basic custom hardware manager which references the `GenericHardwareManager`’s
`{create,delete}_configuration` functions to make them part of automatic cleaning. This way, a user initiated instance deletion will clean up
and re-apply a Software RAID configuration. The longer term plan to increase the flexibility, however, is to integrate the RAID configuration
with the deploy-template framework and in-band deploy steps. This would allow users to define and apply Software RAID configurations themselves,
rather than relying on picking pre-configured nodes from the list of available nodes.

With these additions in place, the support for Software RAID may be capable of fitting even more use cases -- not only at CERN.