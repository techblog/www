---
title: "Container Storage and CephFS (Part 3): Scale testing"
subtitle: "Scaling to 10'000 concurrent clients"
date: 2018-11-30T09:00:03+02:00
author: Róbert Vašek, Ricardo Rocha
tags: ["cephfs", "containers", "csi", "kubernetes", "manila", "openstack" ]
---

[Part 1]({{< ref "container-storage-cephfs-csi-part1" >}}) and [Part 2]({{< ref "container-storage-manila-provisioner-part2" >}}) covered CSI and the CephFS/Manila drivers. In the third part of our recent [train wreck to train ride](https://www.openstack.org/summit/berlin-2018/summit-schedule/events/21997/dynamic-storage-provisioning-of-manilacephfs-shares-on-kubernetes) series we summarize the results of a large test against Kubernetes/CSI and CephFS clusters.

{{< figure src="../../img/post/100-100-idle-mds-sessions.png"
      caption="Spoiler: scaling test with 10'000 concurrent clients"
      class="caption"
>}}

## Overview

Our use cases often include 1000s or 10000s concurrent jobs, all accessing
large amounts of data. For this reason any new component being integrated needs
to cope with this sort of scale. The goals of this effort include:

1. Validate the CSI CephFS implementation
2. Validate the Manila provisioner implementation
3. Scale test the CSI CephFS driver

### Methodology

The first step is to provision **s** CephFS shares using the Manila
provisioner, described in [Part 2]({{< ref "container-storage-manila-provisioner-part2" >}}).
Next we need to create a *Deployment* with **r** replicas (so that we get one
replica per cluster node), with each replica mounting all **s** shares. This
results in **s * r** total mounts across the cluster.

From the CephFS side, each mount is seen as an independent client, which allows
us to nicely scale out our tests by playing with the number of shares and
nodes in the cluster.

The test is launched and managed using [this helm chart](https://gitlab.cern.ch/cloud-infrastructure/csi-benchmark),
feel free to try it out if you want to reproduce our tests in your own
environment. We've run two types of tests:

* **idle**: clients just sit idle ( still keeping a session on CephFS )
* **busy**: clients untar the linux kernel into each mounted share,
  concurrently

The second test type puts significant load in the CephFS MDS instances.

### Cluster Setup

The Kubernetes cluster where the client workloads are deployed is as follows:

* 100 nodes (all virtual machines)
* 2 cores, 4GB RAM nodes (including the master)
* Kubernetes 1.11 and later 1.12.1, CSI CephFS 0.3.1

We've run our tests against two distinct CephFS clusters.

> Learning on the way that our CEPH team is a big fan of The Office tv series

#### Dwight

This is our small CephFS test cluster, mostly used for use case validation. Thanks to
this we hoped we could trigger some failures on the Ceph side and validate the CSI driver
handles them appropriately.

* 3 x 24 HDD OSDs
* 3 MDS, plus one on stand-by
* Ceph Luminous Bluestore

#### Jim

This is serving our HPC cluster, which recently entered the [IO-500 list](https://www.vi4io.org/io500/start)
(hurray!). We got a slot where we could redo our idle tests (with similar
results), and will try a future slot for the busy clients tests.

* 300 SSD OSDs
* 2 MDS
* Ceph Luminous Bluestore
* Hyper-converged setup (shared compute and storage)

### Results

Jump directly to *attempt #3* if you're only interested in the final results.
Below we include the intermediate results and data.

#### Idle Attempt #1: 1'000 Clients

For the first attempt we launched 100 replicas all mounting 10 CephFS shares,
resulting in 1000 concurrent idle clients. Some things went well:

* Provisioning of shares worked as expected
* Some pods survived

*Some* was not what we were looking for, and we saw errors like:
```bash
[200~Aug 30 09:51:45 cci-cephfs-scale-003-n3tf4nqlzisk-minion-56.cern.ch
runc[3255]:
E0830 09:51:45.410380 3270 csi_attacher.go:137] kubernetes.io/csi:
attacher.WaitForAttach failed for volume [pvc-c5848f32-ac39-11e8-bbfb-02163e01b7c5] (will continue to try): volumeattachments.storage.k8s.io "csi-4f2dbe5cb257e7d7b172c4a1e6a1d26bfff82dabeb91e441c527d46f368f1615" is forbidden: User "system:node:cci-cephfs-scale-003-n3tf4nqlzisk-minion-56" cannot get volumeattachments.storage.k8s.io at the cluster scope: no path found to object
```

```bash
User "system:node:NODE_NAME" cannot get volumeattachments at the cluster scope:
no path found to object
```

By this time we realized volume attachments were causing most of the issues, and that
we don't necessarily need them. But skipping attachments required a few changes in
the Kubernetes CSI implementation, which were coming soon but not quite there yet.

#### Idle Attempt #2: If you can't fix it, go around the problem?

While working on the final solution, we added a retrial mechanism on the Pod
creation which would insist until all CephFS shares would be correctly mounted
on each replica.

{{< figure src="../../img/post/plot-mds-attempt2.png"
      caption="Better... but not great"
      class="caption"
>}}

From the plot above we see we reached 655 concurrent clients - with 2 active
MDSs the number of sessions is doubled. This is much better, but still nowhere
close to where we wanted to be.

#### Idle Attempt #3: Third time's the charm?

This time we had a few significant changes that would help:

* Kubernetes 1.12, with driver-registrar 0.4
* Kubelet plugin registration of CSI drivers
* CSISkipAttach

Enabling the last flag was the key to scale out our clients. The default
deployment of a Kubernetes CSI driver will include an external attacher
component waiting for a confirmation of a successful attachment for each *PersistentVolume*.

{{< figure src="../../img/post/csi-skip-attach1.png"
      class="caption"
>}}

An unnecessary step for the CSI CephFS driver, which we could now skip.

{{< figure src="../../img/post/csi-skip-attach2.png"
      class="caption"
>}}

We resumed our tests with 100 CephFS shares and 100 replicas, corresponding to
10'000 concurrent idle clients. Still scaling up gently, in steps of 500
new clients, and finally reaching 10'000 concurrent idle clients:
```bash
15:29:28 : scaling to 5 replicas
...
16:02:26 : scaling to 95 replicas
16:04:16 : scaling to 100 replicas
```

{{< figure src="../../img/post/100-100-idle-mds-sessions.png"
      caption="10'000 concurrent idle clients"
      class="caption"
>}}

The plot shows a nice distribution of client sessions among the 3 MDS instances,
as expected. Leaving the sessions active for a long period of time seemed to
show also perfect behavior, but a quick look at the logs indicated some clients
were being periodically evicted. 
```bash
mds.cephdwightmds2 mds.1 redacted:6800/2246956767 2238 : cluster [WRN] evicting
unresponsive client cci-cephfs-scale-001-3vimalfm74dd-minion-76.cern.ch
(674953366), after 304.623890 seconds
mds.cephdwightmds0 mds.2 redacted:6800/2942371108 2167 : cluster [WRN] evicting
unresponsive client cci-cephfs-scale-001-3vimalfm74dd-minion-76.cern.ch:
(674953366), after 304.2772996 seconds
```

A mix of Kubernetes and the CSI CephFS driver relaunches the workload but in
many cases this won't be good enough so we need to look further into this issue.

We also got plenty of information from the CephFS MDS monitoring which we
started looking into with our in-house Ceph experts. As an example,
even with only idle clients it seems the I/O on the pool is quite significant
which is surprising.

{{< figure src="../../img/post/100-100-idle-cephfs-pool-io.png"
      caption="CephFS Pool I/O"
      class="caption"
>}}

And similarly for the rate of namespace operations:

{{< figure src="../../img/post/100-100-idle-namespaces-rates.png"
      caption="CephFS Namespace Rates"
      class="caption"
>}}

#### Busy clients

In this case each client untars the linux kernel into each CephFS share
it mounts, creating significant load on the CephFS metadata servers. Once more
we slowly ramp up the number of clients, stopping only when it breaks.

{{< figure src="../../img/post/100-100-nonidle-mds-sessions.png"
      caption="Scaling out busy CephFS clients"
      class="caption"
>}}

As this CephFS cluster is rather small, something was expected to break at some point.
In fact this was the main goal of the test, to see how CephFS and the CSI
driver would handle this situation.

At around 200 concurrent clients one of the MDS crashed and we can see in the
plot above that the standby MDS kicked in immediately. From the client side
there was no visible impact, and no trace of any issue in the client logs.

### Wrapping up

It's been a busy year for CSI and Kubernetes, with lots of development building
into [CSI 1.0](https://github.com/container-storage-interface/spec/releases/tag/v1.0.0),
released just a few days ago.

In this series we tried to summarize the aspects of CSI that make it relevant for our infrastructure. We also included
a detailed description of both the CephFS CSI driver and the OpenStack Manila provisioner.

And in this last post we finish with a scale test showing results beating our initial
expectations, and some minor issues to be sorted out in the near future.
If you're interested and wish to contribute, check out github for [csi-cephfs](https://github.com/ceph/ceph-csi)
and the [manila provisioner](https://github.com/kubernetes/cloud-provider-openstack/blob/master/docs/using-manila-provisioner.md).

