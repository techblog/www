---
title: 'Juno, EL6 and RDO Community.'
date: 2015-05-21T09:51:00.001-07:00
draft: false
author: Thomas Oulevey
tags: ["openinfra", "rdo"]
---

Since 2011, CERN selected OpenStack as a cloud platform. It was natural to choose [RDO](https://www.rdoproject.org/) as our RPMs provider ; RDO is a community of people using and deploying OpenStack on Red Hat Enterprise Linux, Fedora and distributions derived from these (such as [Scientific Linux CERN 6](http://linux.web.cern.ch/linux/scientific6/) which powers our hypervisors).  
  
The community decided not to provide official upgrade path from Icehouse to Juno on el6 systems.  
  
While our internal infrastructure is now moving to CentOS 7, we have to maintain during the transition around 2500 compute nodes under SLC6.  
  
As it was mentioned in the previous blog post, we recently finished the migration from Icehouse to Juno. Part of this effort was to rebuild Juno RDO packages for RHEL6 derivative and provide a tested upgrade path from IceHouse.  
  
We are happy to announce that we recompiled openstack-nova and openstack-ceilometer packages publicly with the help of the [CentOS infrastructure](https://wiki.centos.org/HowTos/CommunityBuildSystem) and made them available to the community.  
  
The effort is led by the [CentOS Cloud SIG](https://wiki.centos.org/SpecialInterestGroup/Cloud) and I'd like to thank particularly Alan Pevec, Haïkel Guemar and Karanbir Singh for their support and time.  
  
For all the information and how to use the Juno EL6 packages please follow this link [https://wiki.centos.org/Cloud/OpenStack/JunoEL6QuickStart](https://wiki.centos.org/Cloud/OpenStack/JunoEL6QuickStart).

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

