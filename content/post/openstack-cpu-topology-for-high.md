---
title: 'OpenStack CPU topology for High Throughput Computing'
date: 2015-08-01T02:36:00.003-07:00
draft: false
author: Sean Crosby, Arne Wiebalck
tags: ["openinfra"]
---

We are starting to look at the latest features of OpenStack Juno and Kilo as part of the CERN OpenStack cloud to optimise a number of different compute intensive applications.  
  
We'll break down the tips and techniques into a series of small blogs. A corresponding set of changes to the upstream documentation will also be made to ensure the options are documented fully.  
  
In the modern CPU world, a server consists of multiple levels of processing units.  

*   Sockets where each of the processor chips are inserted
*   Cores where each processors contain multiple processing units which can run multiple processes in parallel
*   Threads (if settings such as [SMT](https://en.wikipedia.org/wiki/Simultaneous_multithreading) are enabled) may allow multiple processing threads to be active at the expense of sharing a core

The typical hardware used at CERN is a 2 socket system. This provides optimum price performance for our typical high throughput applications which simulate and process events from the Large Hadron Collider. The aim is not to process a single event as quickly as possible but rather to process the maximum number of events within a given time (within the total computing budget available). As the price of processors vary according to the performance, the selected systems are often not the fastest possible but the ones which give the best performance/CHF.  
  
A typical example of this approach is in our use of [SMT](https://en.wikipedia.org/wiki/Simultaneous_multithreading) which leads to a 20% increase in total throughput although each individual thread runs correspondingly slower. Thus, the typical configuration is  
 
``` 
# lscpu  
Architecture:          x86_64  
CPU op-mode(s):        32-bit, 64-bit  
Byte Order:            Little Endian  
CPU(s):                32  
On-line CPU(s) list:   0-31  
Thread(s) per core:    2  
Core(s) per socket:    8  
Socket(s):             2  
NUMA node(s):          2  
Vendor ID:             GenuineIntel  
CPU family:            6  
Model:                 62  
Model name:            Intel(R) Xeon(R) CPU E5-2650 v2 @ 2.60GHz  
Stepping:              4  
CPU MHz:               2999.953  
BogoMIPS:              5192.93  
Virtualization:        VT-x  
L1d cache:             32K  
L1i cache:             32K  
L2 cache:              256K  
L3 cache:              20480K  
NUMA node0 CPU(s):     0-7,16-23  
NUMA node1 CPU(s):     8-15,24-31
```

By default in OpenStack, the virtual CPUs in a guest are allocated as standalone processors. This means that for a 32 vCPU VM, it will appear as  
  

*   32 sockets
*   1 core per socket
*   1 thread per socket

As part of ongoing performance investigations, we wondered about the impact of this topology on CPU bound applications.

  

With OpenStack Juno, there is a mechanism to pass the desired topology. This can be done through flavors or image properties.

  

The names are slightly different between the two usages, with flavors using properties which start hw: and images with properties starting hw_. 

  

The flavor configurations are set by the cloud administrators and the image properties can be set by the project members. The cloud administrator can also set maximum values (i.e. hw\_max\_cpu_cores) so that the project members cannot define values which are incompatible with the underlying resources.

```bash  
$ openstack image set --property hw\_cpu\_cores=8 --property hw_cpu_threads=2 --property hw_cpu_sockets=2 0215d732-7da9-444e-a7b5-798d38c769b5  
```
  
The VM which is booted then has this configuration reflected.  

```  
# lscpu  
Architecture:          x86_64  
CPU op-mode(s):        32-bit, 64-bit  
Byte Order:            Little Endian  
CPU(s):                32  
On-line CPU(s) list:   0-31  
Thread(s) per core:    2  
Core(s) per socket:    8  
Socket(s):             2  
NUMA node(s):          1  
Vendor ID:             GenuineIntel  
CPU family:            6  
Model:                 62  
Stepping:              4  
CPU MHz:               2593.748  
BogoMIPS:              5187.49  
Hypervisor vendor:     KVM  
Virtualization type:   full  
L1d cache:             32K  
L1i cache:             32K  
L2 cache:              4096K  
NUMA node0 CPU(s):     0-31
```
  

While this gives the possibility to construct interesting topologies, the performance benefits are not clear. The standard [High Energy Physics benchmark](http://w3.hepix.org/processors/) show no significant change. Given that there is no direct mapping between the cores in the VM and the underlying physical ones, this may be because the cores are not pinned to the corresponding sockets/cores/threads and thus Linux may be optimising for a virtual configuration rather than the real one.

  

This work was in collaboration with Sean Crosby (University of Melbourne) and Arne Wiebalck (CERN).

  
The following documentation reports have been raised  

*   Flavors Extra Specs -  https://bugs.launchpad.net/openstack-manuals/+bug/1479270
*   Image Properties - https://bugs.launchpad.net/openstack-manuals/+bug/1480519

References
----------

*   Recent 2017 documentation is now at h[ttp://docs.openstack.org/admin-guide/compute-adv-config.html](ttp://docs.openstack.org/admin-guide/compute-adv-config.html)

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

