---
title: "Scaling Ironic with Conductor Groups"
date: 2020-05-26T09:00:00+02:00
draft: false
author: Belmiro Moreira, Arne Wiebalck
tags: ["openstack", "nova", "ironic"]
---

CERN has introduced OpenStack Ironic for bare metal provisioning as a production service in 2018. Since then, the service has grown to manage more than 5000 physical nodes and is currently used by all IT services still requiring physical machines. This includes storage or database services, but also the infrastructure for compute services. Even the “compute nodes” used by OpenStack Nova are instances deployed via Nova and Ironic (but that will be a different blog post!). The objective is to have all physical hardware deployed and managed by Ironic, and while the life-cycle of new hardware is largely managed by Ironic already, we will need to have an adoption campaign to enroll the remaining servers (also this will need a different blog post!). With the growth of Ironic we hit various scaling issues which we already described in a [previous post](https://techblog.web.cern.ch/techblog/post/nova-ironic-at-scale/). This post will describe how we reduced the instance creation time by splitting our deployment into conductor groups.

One of the biggest issues that we face when increasing the number of physical nodes managed by Ironic is the time that the Resource Tracker (RT) inside the nova-compute service takes to loop through all the resources. The task of the RT is to update the OpenStack Placement service with the current state of the resource providers, e.g. which nodes are currently available for instantiation.

This update is a sequential and blocking operation, i.e. all operations (create, delete ironic instances, update node status, ...) are blocked until this cycle finishes. The time that the cycle takes scales linearly with the number of nodes. In our deployment with ~5000 nodes it took ~3h to complete.

Our Nova/Ironic deployment architecture is to blame for this long duration to some degree: all physical nodes are managed via a single “bare metal” cell where we only had a single "nova-compute" node that interacts with Ironic. Figure 1 illustrates this initial setup.

<br />

| ![conductor-groups-1.png](../../img/post/conductor-groups-1.png) |
|:--:|
| *Fig. 1 - Original OpenStack Nova and Ironic setup for the dedicated Bare Metal cell* |

<br />

Ironic allows us to have multiple "nova-compute" nodes. However, this is based on a hashring and when we tested this functionality we observed several issues. This was before we upgraded to "Stein" release, though. So, we opted to have the simplest setup possible, only one "nova-compute" node. Sure, this is a bottleneck and a single point of failure, but it's easy to manage, which is also important when scaling an infrastructure.

The “Stein” release of Ironic and Nova introduced the possibility to have "conductor groups" which allows to split the Infrastructure and have failure domains. With conductor groups, we can map "nova-compute" nodes to a set of Ironic nodes. In this respect it is similar to cells in Nova.

## How it works

A conductor group is an association between a set of physical nodes and a set of (one or more) Ironic conductors which manage these physical nodes. This association reduces the number of nodes a conductor is looking after and hence key in scaling the deployment.

The conductor group an Ironic conductor is taking care of is configured in Ironic’s configuration file “ironic.conf” on each Ironic conductor:

```
[conductor]
conductor_group = MY_CONDUCTOR_GROUP
```

The physical Ironic nodes can now be mapped to a conductor group as well.

```
openstack baremetal node set --conductor-group "MY_CONDUCTOR_GROUP" <node_uuid>
```

Ironic nodes without a conductor group defined can only be managed by Ironic conductors that do not manage a specific conductor group. Otherwise, they will be orphaned and not managed at all.

Finally, we need to configure each group of "nova-compute" nodes to manage only a conductor group. This is done in Nova’s configuration file “nova.conf” for each "nova-compute" node:

```
[ironic]
partition_key = MY_CONDUCTOR_GROUP
peer_list = LIST_OF_HOSTNAMES
```

The "partition_key" is where the conductor group is defined and the "peer_list" defines the set of (one or more) "nova-compute" nodes that will manage this particular conductor group. Depending on what you want to achieve, it's important to note that if the "partition_key" is not defined the "nova-compute" will try to manage all the nodes.

## How do we deployed Conductor Groups

We have an Infrastructure with more than 5000 Ironic nodes running in production. The introduction of conductor groups presented some challenges, mainly because we wanted to minimize the service downtime and because we have thousands of Ironic instances running.

Based on some testing we performed we decided to split the infrastructure into groups of ~500 Ironic nodes each: for groups of this size the "nova-compute" resource tracker cycle takes ~15 minutes to complete. This seemed a good compromise for our specific use case and our user expectations. It's expected that the creation of a bare metal node can take a few minutes. ~500 Ironic nodes per group also gives us a much better initial service resilience without compromising the simplicity of the setup. All conductor groups are managed by a single controller, except for the “leading” group which is where new hardware will be added next. These additional wing conductors will share the load, for instance when new instances are created. For Nova, and based on our experience with cells, we decided to have one "nova-compute" node per conductor group. The target layout for the bare metal cell is shown in Figure 2.

<br />

| ![conductor-groups-2.png](../../img/post/conductor-groups-2.png) |
|:--:|
| *Fig. 2 - Layout of the Bare Metal cell after the split* |

<br />

## The procedure

We started by stopping the "nova-compute" service dedicated to Ironic and all the Ironic APIs and conductors. The main reason was to avoid Nova removing nodes after they move to a conductor group.

The newly deployed Ironic conductors were configured with the correct conductor group. We simply selected a sequential conductor group name to easy management (group_001 to group_010)

For example:

```
[conductor]
conductor_group = group_001
```

Then, we split Ironic nodes by the different conductor groups. Performing an API call for each ~5000 Ironic nodes to set the correct conductor group would have taken a lot of time, so we decided to update these entries directly in the Ironic database:

For example:

```
update nodes set conductor_group="group_001", conductor_affinity=24 where uuid in (...)

```

The id to set the conductor_affinity was taken from the conductors table and is the one of the controller assigned to this conductor group.

Finally, it was time to configure Nova. We started by updating the “compute_nodes” table of the Nova cell dedicated to Ironic to match the correct “conductor group”.

```
update compute_nodes set host="NEW_NOVA_COMPUTE" where uuid in (...)
```

All instances in Nova are associated with a compute node. Because we are changing the “nova-compute” nodes that are managing the Ironic nodes it’s important that this change is reflected in the instance. Otherwise, Nova would remove the existing association with the Ironic node.

```
update instances set host="NEW_NOVA_COMPUTE" where uuid in (...)
```

At the end, all “nova-compute” services and Ironic APIs/conductors services were restarted.

<br />

| ![conductor-groups-3.png](../../img/post/conductor-groups-3.png) |
|:--:|
| *Fig. 3 - The nodes per Ironic controller during the intervention* |

<br />

| ![conductor-groups-4.png](../../img/post/conductor-groups-4.png) |
|:--:|
| *Fig. 4 - RT cycle of after enabling conductor groups* |

<br />

## Conclusion

Due to its current locking scheme, the “nova-compute” resource tracker in "Stein" is a bottleneck when using the Ironic driver. It blocks actions such as instance creation when running and hence has a direct (negative) impact on the user experience. By introducing conductor groups we were able to split this locking and reduce the effective lock time from 3 hours to 15 minutes (and even shorter in the “leading” group). This was an improvement very visible to users! Equally, the conductor groups improved the service resilience as there are now multiple failure domains while keeping the service layout is still relatively simple and manageable.

In short, this change provided us a way to overcome one of the scaling limitations of our Ironic deployment and now paves the way for the adoption of the remaining physical servers in CERN IT.
