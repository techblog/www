---
title: 'The First Week - Projects'
date: 2013-08-01T00:18:00.001-07:00
draft: false
author: Tim Bell
tags: ["openinfra"]
---

The First Week - Hot Topics - Projects!
---------------------------------------

At CERN, we've recently gone live with our OpenStack based cloud for 11,000 physicists around the world.  
  
The major efforts prior to going live were to perform the integration into the CERN identity management system, define projects and roles and configure the components in a high availability set up using the Puppetlabs tools. Much of this work was generic and patches have been submitted back to the community.  
  
Surprisingly, the major topics on our cloud go-live day were not how to implement applications using cloud technologies, how accounting is performed or support levels for non-standard images.  
  
Instead, the support lines were hot with two topics... flavors and projects! I'll cover flavors in a subsequent posting.  
  
For projects, each user signing up to CERN private cloud is given an personal quota of 10 cores so they can work in a sandbox to understand how to use clouds. The vast majority of resources will be allocated to shared projects where multiple users collaborate together to simulate physics and analyse the results from the Large Hadron Collider to match with the theory.  
  
Users want a descriptive name for their project. Our standard request form asks  

*   What is the name of your project ?
*   How many cores/MB memory/GB disk/etc would you like ?
*   Who is the owner for the project ? 
*   Who are the administrators ?

These do not seem to be [Bridge of Death](http://www.youtube.com/watch?v=pWS8Mg-JWSg) questions but actually require much reflection.  
  
From the user's perspective, the name should be simple such as 'Simulation'. From the cloud provider's needs, we have multiple user groups to support so the name need to be unique and clear.  
With OpenStack, there are upcoming concepts such as domains which will allow us to group a set of projects together and we wish to prepare the ground for those. So, there is a need for fine grain project definition awaiting future methods to group these projects together.  
  
In the end, we settled on  

*   Projects start with the LHC experiment such as ATLAS or CMS. The case reflects whether they are acronyms or not (ATLAS stands for A Toroidal LHC Apparatus, CMS for Compact Muon Solenoid which is ironic for something weighing over 12,000 tonnes)
*   Many requests asked for _ or - in the names. We prefer spaces. There will be bugs (we've already fixed one in the dashboard) but this is desirable in the long term.
*   For projects run by the IT department, we use our service catalog based on the ITIL methodology so that each functional element can request a project.

Keeping projects small and focused has a number of benefits  

*   The accounting can be more precise as the association between a project and a service is clearer.
*   The list of members for a project can be kept small. With the Grizzly based OpenStack cloud, the policies allow members to restart other VMs in the project. This allows sharing of administration responsibilities but can cause issues if multiple teams are members of a single project.

The disadvantages are

*   Additional administration to set up the projects
*   Dependencies on the upcoming domain features in Keystone which should be arriving in the Havana release
*   Quota management is more effort for the support teams. With large projects, the central team can allocate out a large block of quota to a project and leave the project team to look after the resources.

Ultimately, there is a need for an accounting structure of projects and domains so we can track who is using what. Getting the domain/project structure right at the start feeds into the quota management and accounting model in the future.  
  
The good news on the projects and quotas is that Havana has some interesting improvements which will improve the support load

*   CERN and HP have been working on delegated quota management (see [https://wiki.openstack.org/wiki/DomainQuotaManagementAndEnforcement](https://wiki.openstack.org/wiki/DomainQuotaManagementAndEnforcement))
*   As Domains become a standard part of Keystone with the v3 API, we'll be able to group these smaller projects together for reporting and role management

Notes
-----

Migrated from [openstack-in-production blog spot](https://openstack-in-production.blogspot.com/2013/08/the-first-week-projects.html)