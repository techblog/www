---
title: "The Case of Ironic in CERN IT"
date: 2021-04-23T11:00:00+02:00
draft: false
author: Arne Wiebalck
tags: ["openstack", "nova", "ironic"]
---

At a recent call with other data centre operators to discuss their plans for a new computing facility, we were asked to explain why we use Ironic for the management of physical servers in CERN IT. Back-end services, such as Ironic, may face this question more often than others which have direct user interaction (and certainly more than tools which surf on the top of their hype curve): the gain may not be so apparent which leads to being perceived as yet another layer in the stack. So, the question is very valid and as this was not the first time we got it, this post will try to make a case for the reasons CERN IT relies on ‘behind-the-scenes’ OpenStack Ironic for bare metal provisioning.

(To get an idea what Ironic actually does, have a look at the [Ironic Website](http://ironicbaremetal.org).


## Wanted: Physical Servers!

CERN IT successfully moved to an on-premise cloud, based on OpenStack, [almost 10 years ago](https://techblog.web.cern.ch/techblog/post/10-years-of-openstack-at-cern-part-1/). The concomitant policy introduced back then was to have all resources provisioned as virtual machines. While this worked out for almost all use cases, there was always a need to provision bare metal servers in addition: for some services, it wasn’t economic to use virtual machines (e.g. when full node VMs were required), it did not make sense technically (e.g. for storage servers), it introduced a potentially undesired dependency or lack of control (e.g. core networking services), or it was simply not feasible (e.g. cloud hypervisors). Equally, some use cases could not be fulfilled by virtual machines, e.g. evaluating fine-grained performance tuning which would be disturbed by crosstalk from co-hosted instances. In some cases, like the batch compute service, the choice of virtual vs. physical resources was far from obvious (the gain of having an API to manage resources enabling corresponding automation outweighed eventually the performance loss due to the virtualization tax), and could have gone the other way as well. So, as use cases add up, the need for bare metal provisioning workflows and a bare metal management engine never went away. And clearly, at least initially, the option of a strong integration of Ironic with OpenStack was the main reason to select Ironic.

## Consolidated resource provisioning

With several thousand users in our cloud infrastructure, we needed to build an automatic request/approval/apply workflow for virtual resources pretty soon after the initial deployment. In order to request resources, users specify the amount of {cores, RAM, storage, load balancers, …} via a form which produces a record. This record is sent for approval to the corresponding resource manager and can, once approved, be applied with the click of a button. The tools we use in CERN IT for this are our generic ticketing system based on [ServiceNow](http://servicenow.com) and [Rundeck](https://www.rundeck.com/). Presenting the user a single interface to request resources, virtual and physical, clearly promises a better user experience than offering different entry points. With the integration into OpenStack, the exact same workflow we built for virtual machines can now be used for physical resources as well. Approved requests will appear as an increased quota and access to the corresponding flavor on an OpenStack project. There is still a difference to virtual machines, since physical machines can only be provisioned when they are available. So, larger requests still need a corresponding preparation. Smaller requests, however, can be handled from a pool of free physical nodes. In both cases, the actual handover to the user or service happens through OpenStack and Ironic.
A special case is the reallocation of physical servers from one user or service to another, a workflow not needed for virtual machines. Prior to Ironic, the nodes were given back to the hardware team, the entries in various internal databases needed to be updated, the nodes underwent some cleaning and checking, and were then handed over to the new user. With Ironic, the current user deletes the instances in OpenStack (which triggers auto-cleaning via Ironic), and once the nodes have finished and appear as available again, the new user creates new instances. Apart from (potentially) granting flavor access, there is no direct admin involvement anymore.

## Simplified accounting

Another aspect that was simplified by using a tool integrated with OpenStack is accounting. With the move to an OpenStack cloud, resource accounting started to be based on OpenStack projects. Having physical resources appear as instances as well allowed to reduce the number of sources for the accounting tool chain (some care has to be taken to not double count physical instances which host virtual instances). Furthermore, all physical nodes are listed in Ironic, along with their instances (and therefore their projects and users). This eliminates any potential ambiguity when it comes to resource allocation and ownership. The support for multi-tenancy and a owner/lessee model (or the upcoming support in Nova for this) will help even more.

## Leverage existing virtual machines workflows

Does the user benefit from Ironic beyond the initial resource provisioning which is now identical to the one for virtual machines? Ironic is a service which is not directly user-facing and for users all Ironic interaction happens through the OpenStack Compute (Nova) interface. However, since the user-facing API is the same for virtual and physical servers, commands and tools using OpenStack to interact with virtual machines, will now also work with physical machines. Simple examples are the commands to reboot a server or to get access to the console: `openstack server reboot ...` or `openstack console url show ...`. A more sophisticated example where we leverage this API equality in CERN IT is OpenStack Magnum, which is a service providing clusters for container orchestration engines such as Kubernetes. With templates using bare metal flavors, instead of virtual ones, Magnum is now capable of creating clusters on bare metal nodes instead of virtual machines, or even mixed clusters. Another example is the management of larger resource sets, such as in the batch service which included more than 20’000 virtual machines. The processes to create and delete virtual instances, e.g. when new resources have become available or when notifications for upcoming interventions appear on the message bus, could be re-used to handle similar events for physical machines. All this is true within certain limits of course: physical machines still behave differently, they need longer to be provisioned, require cleaning upon deletion, are more susceptible to failures etc., but the majority of workflows still apply.

## Life-cycle management

Once Ironic was established for its initial use case of bare metal provisioning, we started to look into how to move some of our server life-cycle management to it as well. This includes steps like:
* (auto-) registration, in Ironic: auto-discovery and inspection
* health/inventory checking, in Ironic: introspection rules
* burn-in, in Ironic: a new step for manual cleaning
* benchmark, in Ironic: a new step for manual cleaning
* configure, this concerns the software RAID setup and is supported in Ironic
provision
* adopt, this means the transparent integration of existing in-production nodes into Ironic and is covered in a separate [blog post](https://techblog.web.cern.ch/techblog/post/ironic-nova-adoption/)
* repair, in Ironic: maintenance mode
* retire, in Ironic: retirement flag

{{< figure src="../../img/post/ironic_life_cycle_management.png"
     caption="Fig. 1 - Life-cycle Phases of physical servers in CERN IT and their management with Ironic"
     class="caption"
>}}

The already existing features in Ironic, such as auto-discovery, the cleaning framework or the introspection rules, allowed us to revisit most of our in-house built tools for the life-cycle management of physical machines. Eventually, this  will remove some current duplication, such as different RAM disks for different phases of the life-cycle, and move the whole workflow to a single, community-based tool. Additions, such as an integration with [OpenDCIM](https://opendcim.org/), is in the making, pulling introspection data from the S3 backend where Ironic stores it. With Ironic’s manual inspection, this would even provide regularly (or on-demand) updated hardware inventory information.

## Open-source software and community interaction

One important aspect when introducing a new service is product adaptability, evolution, and longer term maintenance. Obviously, not all requirements we had in CERN IT were met by Ironic right away. Examples include the support for software RAID, a retirement mechanism, or more complex introspection rules. Equally, when our deployment grew from a few hundred to several thousand nodes, we hit various scaling issues. Some of these could be mitigated with existing features, such as conductor groups, for others we provided patches, such as inspector leader election or database lazy-loading. In all cases, the upstream community was extremely helpful to understand issues and needs, and constructive to get these sorted. Being able to tap into the brain and to leverage the experience of a world-wide community is something we always cite when arguing for this approach to service design, and it certainly worked out here.

## Why, oh, why?

So, back to the “Why?”. As should have become clear by now, CERN IT initially picked Ironic for bare metal provisioning and accounting due to its support of a strong integration with OpenStack: we were part of the community already, had several components deployed and several years of experience running Openstack services in production. Moreover, our users were familiar with the OpenStack interfaces and the associated workflows. Over time, however, Ironic in CERN IT has grown (and is still growing) into a bare metal management framework, enabling Infrastructure-as-code tools, such as [Terraform](https://www.terraform.io/), on top, while handling the full life-cycle of physical servers in CERN IT, from initial registration to final retirement, below. In addition to moving away from in-house built tools towards community-based software, Ironic’s capability (and the community’s willingness) to extend its functionality to support more use cases is probably the main reason we use Ironic in CERN IT today.

## Tell me more!

To learn more about Ironic or get answers to concrete questions, we recommend the following community resources:

* the [Ironic website](http://ironicbaremetal.org) -- best for an overview, how to start, and find the documentation!
* the [openstack-discuss@lists.openstack.org](mailto:openstack-discuss@lists.openstack.org) mailing list, tagged with “[ironic]” in the subject -- this is best for reach!
* the #openstack-ironic channel on [freenode](https://freenode.net/) -- this is best for direct access to developers/experts and quick questions/discussions!
* the [Bare Metal SIG](https://etherpad.opendev.org/p/bare-metal-sig) -- this is best to meet other operators and to exchange experiences!
