---
title: 'OpenStack papers community on Zenodo'
date: 2017-06-06T10:30:00.005-07:00
draft: false
author: Tim Bell
tags: ["openinfra"]
---
  
At the recent summit in Boston, Doug Hellmann and I were discussing research around OpenStack, both the software itself but also how it is used by applications. There are many papers being published in proceedings of conferences and PhD theses but finding out about these can be difficult. While these papers may not necessarily lead to open source code contribution, the results of this research is a valuable resource for the community.  
  
Increasingly, publications are made with [Open Access](https://en.wikipedia.org/wiki/Open_access) conditions which are free of all restrictions on access. For example, all projects receiving European Union Horizon 2020 funding are required to make sure that any peer-reviewed journal article they publish is openly accessible, free of charge. Reviewing with the OpenStack scientific working group, Open access was also felt to be consistent with OpenStack's [Open principles](https://wiki.openstack.org/wiki/Open) of Open Source, Open Design, Open Development and Open Community.  
  
There are a number of different repositories available where publications such as this can be made available. The OpenStack scientific working group are evaluating potential approaches and [Zenodo](http://zenodo.org/) looks like a good candidate as it is already widely used in the research community, [open source](https://github.com/zenodo) on github and the application also runs in the CERN Data Centre on OpenStack. Preservation of data is one of CERN's key missions and this is included in the service delivery for Zenodo.  
  
The name Zenodo is derived from Zenodotus, the first librarian of the Ancient Library of Alexandria and father of the first recorded use of metadata, a landmark in library history.  

Accessing the Repository
------------------------

The list of papers can be seen at [https://zenodo.org/communities/openstack-papers](https://zenodo.org/communities/openstack-papers). Along with keywords, there is a dedicated search facility is available within the community so that relevant papers can be found quickly.

Submitting New Papers
---------------------

Zenodo allows new papers to be submitted for inclusion into the OpenStack Papers repository. There are a number of steps to be performed.  
  
Please ensure that these papers are available under open access conditions before submitting them to the repository if published elsewhere. Alternatively, if the papers can be published freely, they can be published in Zenodo for the first time and receive the DOI directly.

1.  Log in to [Zenodo](http://zenodo.org/). This can be done using your github account if you have one or by registering a new account via the 'Sign Up' button.
2.  Once logged in, you can go to the openstack repository at [https://zenodo.org/communities/openstack-papers](https://zenodo.org/communities/openstack-papers) and upload a new paper.
3.  The submission will then be verified before publishing.

To submit for this repository, you need to provide

*   Title of the paper
*   Author list
*   Description (the Abstract is often a good content)
*   Date of publication

If you know the information, please provide the following also

*   [DOI](https://en.wikipedia.org/wiki/Digital_object_identifier) (Digital Object Identifier) used to uniquely identify the object. In general, these will already be allocated to the paper since the original publication will have allocated one. If none is specified, Zenodo will create one, which is good for new publications but bad practice to generate duplicate DOIs for published works. So please try to find the original, which also it helps with future cross referencing.
*   There are optional fields at upload time for adding more metadata (to make it machine readable), such as “Journal” and “Conference”. Adding journal information improves the searching and collating of documents for the future so if this information is known, it is good to enter it.

Zenodo provides the synchronisation facilities for repositories to exchange information ([OAI 2.0](https://zenodo.org/oai2d?verb=ListRecords&set=user-openstack-papers&metadataPrefix=oai_dc)). Planet OpenStack feeds using this would be an interesting enhancement to consider or adding RSS support to Zenodo would be welcome contributions.

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

