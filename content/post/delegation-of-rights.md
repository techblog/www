---
title: 'Delegation of rights'
date: 2015-02-17T02:53:00.000-08:00
draft: false
author: Tim Bell, Jose Castro Leon
tags: ["openinfra", "keystone"]
---

At CERN, we have 1st and 2nd level support teams to run the computer centre infrastructure. These groups provide 24x7 coverage for problems and initial problem diagnosis to determine which 3rd line support team needs to be called in the event of a critical problem. Typical operations required are  

*   Stop/Start/Reboot server
*   Inspect console

When we ran application services on physical servers, these activities could be performed using a number of different technologies

*   KVM switches
*   IPMI for remote maagement
*   Power buttons and the console trolley

With a virtual infrastructure, the applications are now running on virtual machines within a project. These operations are not available by default for the 1st and 2nd level teams since only the members of the project can perform these commands. On the other hand, the project administrator rights contain other operations (such as delete or rebuild servers) which are not needed by these teams.

  

To address this, we have defined an OpenStack policy for the projects concerned. This is an opt-in process so that the project administrator needs to decide whether these delegated rights should be made available (either at project creation or later).

  

### Define operator role

The first step is to define a new role, operator, for the projects concerned. This can be done through the GUI (http://docs.openstack.org/user-guide-admin/content/section\_dashboard\_admin\_manage\_roles.html) or via the CLI (http://docs.openstack.org/user-guide-admin/content/admin\_cli\_manage\_projects\_users.html). In CERN's case, we include it into the workflow in the project creation.

  

On a default configuration,

  
```bash
$ keystone role-list
+----------------------------------+---------------+
|                id                |      name     |
+----------------------------------+---------------+
| ef8afe7ea1864b97994451fbe949f8c9 | ResellerAdmin |
| 8fc0ca6ef49a448d930593e65fc528e8 | SwiftOperator |
| 9fe2ff9ee4384b1894a90878d3e92bab |    _member_   |
| 172d0175306249d087f9a31d31ce053a |     admin     |
+----------------------------------+---------------+
```
  

A new role operator needs to be defined, using the steps from the [documentation](http://docs.openstack.org/admin-guide-cloud/content/keystone-user-management.html). 

```bash
 $ keystone role-create --name operator
+----------+----------------------------------+
| Property |              Value               |
+----------+----------------------------------+
|    id    | e97375051a0e4bdeaf703f5a90892996 |
|   name   |             operator             |
+----------+----------------------------------+
```
  

and the new role will then appear in the keystone role-list.

  

Now add a new user operator1

```bash
$ keystone user-create --name operator1 --pass operatorpass
+----------+----------------------------------+
| Property |              Value               |
+----------+----------------------------------+
|  email   |                                  |
| enabled  |               True               |
|    id    | f93a50c12c164f329ee15d4d5b0e7999 |
|   name   |            operator1             |
| username |            operator1             |
+----------+----------------------------------+
```
  

and add the  operator1 account to the role

```bash
$ keystone user-role-add --user operator1 --role operator  --tenant demo
$ keystone user-role-list --tenant demo --user operator1
```
  

A similar role is defined for accounting which is used to allow the CERN accounting system read-only access to data about instances so that an accounting report can be produced without needing OpenStack admin rights.  
  
For mapping which users are given this role, we use the Keystone V3 functions available through the OpenStack unified CLI.  
  
```
$ openstack role add --group operatorGroup --role operator  --tenant demo
```
  

Using a group operatorGroup, we are able to define the members in Active Directory and then have those users updated automatically with consistent role sets. The users can also be added explicitly  
```  
$ openstack role add --user operator1 --role operator  --tenant demo  
```  

### Update nova policy

The key file is called policy.json in /etc/nova which defines the roles and what they can do. There are two parts to the rules, firstly a set of groupings which give a human readable description for a complex rule set such as a member is someone who is not an accounting role and not an operator:

```  

    "context\_is\_admin":  "role:admin",

    "context\_is\_member": "not role:accounting and not role:operator",

    "admin\_or\_owner":  "is\_admin:True or (project\_id:%(project\_id)s and rule:context\_is_member)",

    "default": "rule:admin\_or\_owner",

    "default\_or\_operator": "is\_admin:True or (project\_id:%(project_id)s and not role:accounting)",

```  

The particular rules are relatively self-descriptive.  
  
The actions can then be defined using these terms

  
```
    "compute:get":"rule:default\_or\_operator",

    "compute:get\_all": "rule:default\_or_operator",

    "compute:get\_all\_tenants": "rule:default\_or\_operator",

    "compute:stop":"rule:default\_or\_operator",

    "compute:start":"rule:default\_or\_operator",

    "compute:reboot":"rule:default\_or\_operator", "compute:get\_vnc\_console":"rule:default\_or\_operator",

    "compute:get\_spice\_console":"rule:default\_or\_operator",

    "compute:get\_console\_output":"rule:default\_or\_operator",

    "compute\_extension:console\_output": "rule:default\_or\_operator",

    "compute\_extension:consoles": "rule:default\_or_operator",
```
  

  

With this, a user group can be defined to allow stop/start/reboot/console while not being able to perform the more destructive operations such as delete.

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

