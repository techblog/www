---
title: 'Blogspot to Techblog Hugo migration'
date: 2019-01-22T04:29:00.001-07:00
draft: false
author: Tim Bell
tags : [hugo]
---
 
CERN has been sharing the experiences with OpenStack on the blog http://openstack-in-production.blogspot.com since 2012. This has covered over 40 blogs and many hints and tips for the community.

However, the CERN IT transformation to open source tools covers many other components and thus we are moving to a new home (https://techblog.web.cern.ch) which covers other technologies along with OpenStack.

To migrate, we wanted to be able to export the contents and meta data from blogspot and be able to import it into Hugo, the technology chosen for the techblog. This meant finding a blogspot to Markdown converter which also was able to add the data such as title, date of publication, author and tags. A quick search around the internet produced a few matches and some quick testing led to selecting [blog2md](https://github.com/palaniraja/blog2md).

The initial export was performed from the blogspot management window. Going to Settings/Other, produces an option to backup content. 

{{< figure src="../../img/post/blogspotexport.png"
     caption="Fig. 1 - Export blog contents to XML"
     class="caption"
>}}

Selecting this option will produce an XML file to be downloaded and saved for later. This XML contains all of the blog contents and the needed metadata.

To convert into markdown, blog2md is used. The project was extracted from github and then the following runs the conversion. The directory 'out' has single files for each blog in Markdown.

```
$ node index.js b /Users/timbell/Downloads/blog-01-27-2019.xml out
INFO: Comments requested to be a separate .md file(m - default)
Total no. of entries found : 110
Content-posts 48
Content-Comments 4
title: "OpenStack In Production - moving to a new home"
date: 2019-01-19T01:31:00.000-08:00
draft: false
out/openstack-in-production-moving-to-new.md
No of category: 1
tags : []
{ pid: '2898678996645670571',
  postName: 'out/openstack-in-production-moving-to-new.md',
  fname: 'out/openstack-in-production-moving-to-new-comments.md',
  comments: [] }






DEBUG: going to write to out/openstack-in-production-moving-to-new.md
Successfully written to out/openstack-in-production-moving-to-new.md
```

Most of the conversions ran smoothly but there were a few manual edits required.

### Tables

The markdown table format was not converted smoothly so a set of manual markdown tables were needed based on the content. With some markdown limitations, it was not easy to reproduce such as right align for numbers.

### Images

The images were converted but referred to the content on blogspot. Since the long term aim is no longer need the blogspot content, the images needed to be dowloaded to the techblog git repository and appropriate new links established.

### Tags

Since the new blog has multiple technologies, these old blogs needed to be marked as ones related to OpenStack. However, there is an RSS feed already established for new content on the techblog to be included in the Planet OpenStack aggregator if tagged with openstack. Thus, we tagged the legacy content with openinfra rather than openstack to avoid too many new entries appearing for old content.



