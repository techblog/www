---
title: "Container Storage and CephFS (Part 2): Auto provisioning Manila shares"
subtitle: "Integration of Kubernetes and OpenStack Manila"
date: 2018-11-28T09:00:03+02:00
author: Róbert Vašek, Ricardo Rocha
tags: ["cephfs", "containers", "csi", "kubernetes", "manila", "openstack" ]
---

[Part 1]({{< ref "container-storage-cephfs-csi-part1" >}}) of this series covered CSI, Kubernetes and the CephFS CSI driver. In this second part we look at the integration with [OpenStack Manila](https://wiki.openstack.org/wiki/Manila) and how we auto provision filesystem shares. Check our recent presentation at the OpenStack Summit Berlin - [Dynamic Storage Provisioning of Manila/CephFS Shares on Kubernetes](https://www.openstack.org/summit/berlin-2018/summit-schedule/events/21997/dynamic-storage-provisioning-of-manilacephfs-shares-on-kubernetes), aka "From a train wreck to a train ride".

## Why

In many cases our users will explicitly create their filesystem shares in advance, and will reuse them over
an extended period of time - an example of this would be a filesystem backing
a database. In other cases, they want shares to be auto provisioned and
eventually deleted once the workload has finished - batch jobs or ML model
training often have this requirement, as the input and output data can be quite
large.

The [Manila provisioner](https://github.com/kubernetes/cloud-provider-openstack/blob/master/docs/using-manila-provisioner.md) makes OpenStack Manila shares available directly in
Kubernetes clusters, supporting both modes described above. Once an operator
deploys the provisioner and creates storage classes pointing to OpenStack
credentials, users can define persistent volume claims and get them
attached to their workloads.

At the time of this writing, only CephFS shares are fully supported, with
ongoing work upstream to include NFS and other backends.

## OpenStack Manila Provisioner

Kubernetes is currently incubating an [external-storage](https://github.com/kubernetes-incubator/external-storage) project, which includes helper libraries to build out-of-tree volume provisioners. It is
built around the controller mechanism, listening for events in storage classes,
persistent volume claims and persistent volumes. As a response to these events,
it can create and delete persistent volumes when needed.


The Manila provisioner relies on these libraries and will handle any claim with a storage class provisioner set
to `manila-provisioner`. This is taken by the *Provision* method, allocating the underlying storage assets and
using the helper library to register the volume in Kubernetes. In a similar
manner, the *Delete* method frees the storage assets and unregisters the
volume - whether the share is actually deleted or simply released is set by the
reclaimPolicy.

### Implementation

The code relies heavily on the [gophercloud](https://github.com/gophercloud/gophercloud)
library, which fully supports the Manila API. Authentication to OpenStack can be done using
traditional credentials (user and project), or using trustees.

> The ability to authenticate using a trustee is particularly handy when it comes to deploying Kubernetes with OpenStack Magnum as it creates [trustee relationships automatically](https://specs.openstack.org/openstack/magnum-specs/specs/pre-ocata/implemented/create-trustee-user-for-each-bay.html).
> The cluster operator may configure the deployment to include a default storage class with trustee credentials, minimizing the user setup to consume shares.

Once authenticated, to create a new share the provisioner will first look at
the request type and instantiate a corresponding *share backend*. This backend
is not only share specific, but also specific to the way the volume will be
attached once it's ready. It has two main responsibilities:

* managing the lifetime of any access-related resources: e.g. grant access to the share and create Kubernetes secrets when the share is created; revoke the access and delete the secrets when deleting the share
* create and manage Kubernetes _PersistentVolumeSource_ objects: these are opaque
to Kubernetes but contain volume parameters, secrets and are a part of the persistent volume spec

The volume source is then encapsulated in a _PersistentVolume_ object, passed back to external-storage's _provisionClaimOperation_ and registered in Kubernetes. Such volume is now in _Bound_ state, ready for attaching.

The follow up depends on which driver is serving the volume. In the case of CSI
CephFS, the in-tree CSI volume plugin takes over and the flow follows the
pattern describe in [Part 1]({{< ref "container-storage-cephfs-csi-part1" >}}) of this series.

### Usage

Below we describe the existing setup at CERN.

We have decided to deploy all Kubernetes clusters with pre-defined storage
classes, one for each existing Manila share type. An example:
```yaml
apiVersion: storage.k8s.io/v1beta1
kind: StorageClass
metadata:
  name: meyrin-cephfs
provisioner: manila-provisioner
parameters:
  zones: nova
  type: "Meyrin CephFS"
  osSecretNamespace: kube-system
  osSecretName: os-trustee
  protocol: CEPHFS
  backend: csi-cephfs
  csi-driver: csi-cephfsplugin
```

We point to an existing secret *os-trustee*, which is also deployed by default
and includes the trustee credentials. This simplifies our user's setup
significantly, as they do not need to provide any additional credentials.

The only required step from a user is to define a Persistent Volume Claim, 
choosing the appropriate storage class. An example:
```yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 10G
  storageClassName: meyrin-cephfs
```

In this case, we ask for a share of 10GB backed on our *Meyrin CephFS* cluster.
A simple example using the claim from a nginx deployment:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mynginx
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 1
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        ports:
        - containerPort: 80
        volumeMounts:
          - mountPath: /var/lib/www/html
            name: mypv
      volumes:
      - name: mypv
        persistentVolumeClaim:
          claimName: myclaim
          readOnly: false
```

After only a few seconds, the claim is bound and the container starts:
```bash
$ kubectl get pvc
NAME      STATUS  VOLUME       CAPACITY  ACCESS MODES  STORAGECLASS   AGE
myclaim   Bound   pvc-ab33...  10G       RWO           meyrin-cephfs  1m
```

Before opening it up to all our users, we wanted to make sure these components
would scale to some large use cases. That will be part 3 of this series.

## Upcoming Features

In addition to the CSI CephFS driver, the OpenStack Manila provisioner is also
available in production to all our users. There are some exciting features
coming up, including:

* [Volume resize](https://kubernetes.io/blog/2018/07/12/resizing-persistent-volumes-using-kubernetes/), available in beta since Kubernetes 1.11
* [Snapshotting of existing shares](https://kubernetes.io/blog/2018/10/09/introducing-volume-snapshot-alpha-for-kubernetes/), available in alpha state in Kubernetes 1.12
* [Topology aware volume provisioning](https://kubernetes.io/blog/2018/10/11/topology-aware-volume-provisioning-in-kubernetes/), also available in Kubernetes 1.12

We look forward to continue working together with the CEPH, Kubernetes and
OpenStack upstream communities to develop and validate support for these
features in the coming months.
