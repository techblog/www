---
title: 'Exceeding tracked connections'
date: 2015-01-27T02:21:00.000-08:00
draft: false
author: Tim Bell
tags: ["openinfra"]
---

As we increase the capacity of the CERN OpenStack cloud, we've noticed a few cases of an interesting problem where hypervisors lose network connectivity. These hypervisors are KVM based running Scientific Linux CERN 6. The cloud itself is running Icehouse using Nova network.  
  
  

Connection tracking refers to the ability to maintain state information about a connection in memory tables, such as source and destination ip address and port number pairs (known as _socket pairs_), protocol types, connection state and timeouts. Firewalls that do this are known as _stateful_. Stateful firewalling is inherently more secure than its "stateless" counterpart .... simple packet filtering.

More details are available at [\[1\]](http://www.rigacci.org/wiki/lib/exe/fetch.php/doc/appunti/linux/sa/iptables/conntrack.html).

On busy hypervisors, in the syslog file, we have messages such as
  
```
Jan  4 23:14:44 hypervisor kernel: nf_conntrack: table full, dropping packet.
```

Searching around the internet, we found references to a number of documents [\[2\]](https://wiki.khnet.info/index.php/Conntrack_tuning)[\[3\]](http://www.pc-freak.net/blog/resolving-nf_conntrack-table-full-dropping-packet-flood-message-in-dmesg-linux-kernel-log/) discussing the limit.

  

It appears that the default algorithm is pretty simple. For a 64 bit hypervisor,

*   If RAM < 1 GB, the maximum conntrack is set to RAM in bytes  / 32768
*   Otherwise, set to 65536

Our typical hypervisors contain 48GB of memory and 24 cores so a busy server handling physics distributed data access can easily use 1000s of connections, especially if sockets are not being closed correctly. With several instances of these servers on a hypervisor, it is easy to reach the 65536 limit and start to drop new connections.

  

To keep an eye on the usage, the current and maximum values can be checked using sysctl.

  

The current usage can be checked using

```
# sysctl net.netfilter.nf\_conntrack\_count

net.netfilter.nf\_conntrack\_count = 6650
```
  

The maximum value can be found as follows

  
```
# sysctl net.netfilter.nf\_conntrack\_max

net.netfilter.nf\_conntrack\_max = 65536
```
  

To avoid overload on the hypervisors, the hypervisor conntrack value can be increased and should then be set to the sum of the connections expected from each virtual machine. This can be done using the /etc/sysctl.d directory or with an appropriate configuration management tool.  
  
Note that you'll need to set both net.netfilter.nf\_conntrack\_max as well as  net.nf\_conntrack\_max. For the CERN OpenStack cloud we have increased the values from 64k to 512k.

  

  

\[1\] [http://www.rigacci.org/wiki/lib/exe/fetch.php/doc/appunti/linux/sa/iptables/conntrack.html](http://www.rigacci.org/wiki/lib/exe/fetch.php/doc/appunti/linux/sa/iptables/conntrack.html)

\[2\] [https://wiki.khnet.info/index.php/Conntrack_tuning](https://wiki.khnet.info/index.php/Conntrack_tuning)

\[3\] [http://www.pc-freak.net/blog/resolving-nf_conntrack-table-full-dropping-packet-flood-message-in-dmesg-linux-kernel-log/](http://www.pc-freak.net/blog/resolving-nf_conntrack-table-full-dropping-packet-flood-message-in-dmesg-linux-kernel-log/)[](https://www.blogger.com/)

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

