---
title: 'Deploying the new OpenStack EC2 API project'
date: 2016-04-19T03:35:00.001-07:00
draft: false
author: Tim Bell
tags : [ec2, openinfra, htcondor]
---

OpenStack has supported a subset of the EC2 API since the start of the project. This was originally built in to Nova directly. At CERN, we use this for a number of use cases where the experiments are running across both the on-premise and AWS clouds and would like a consistent API. A typical example of this is the [HTCondor batch system](https://www.google.ch/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwifsJWxt_7LAhWDORQKHbV7CVUQFggcMAA&url=https%3A%2F%2Fresearch.cs.wisc.edu%2Fhtcondor%2F&usg=AFQjCNFT940_-N3dwa_We7sdN9y21dLizQ&sig2=7v4yVZpgjGBPhwU9pBQD-Q&bvm=bv.118817766,d.bGg) which can instantiate [new workers according to demand in the queue](https://research.cs.wisc.edu/htcondor/HTCondorWeek2013/presentations/MillerT_EC2Tutorial.pptx) on the target cloud.  
  
With the [Kilo release](https://wiki.openstack.org/wiki/ReleaseNotes/Kilo#Upgrade_Notes_2), this function was deprecated and has been removed in [Mitaka](http://docs.openstack.org/releasenotes/nova/mitaka.html). The functionality is now provided by the new [ec2-api](http://git.openstack.org/cgit/openstack/ec2-api/) project which uses the public Nova APIs to provide an EC2 compatible interface.  
  
Given that CERN has the goal to upgrade to the latest OpenStack release in the production cloud before the next release is available, a migration to the ec2-api project was required before the deployment of Mitaka, due to be deployed at CERN in 2H 2016.  
  
The EC2 API project was easy to set up using the underlying information from Nova and a small database which is used to store some EC2 specific information such as tags.  
  
As described in [Subbu's blog](https://www.subbu.org/blog/2013/07/openstack-is-not-cloud), there are many parts needed before for an OpenStack API to become a service. By deploying using the CERN cloud, many aspects on identity, capacity planning, log handling, onboarding are covered by the existing infrastructure.  

 
{{< figure src="../../img/post/deployingec2subbu.png"
     caption="Not just installing the RPMs"
     class="caption"
>}}
 
From the CERN perspective, the key functions we need in addition to the code are

*   Packaging - we work with the [RDO](https://www.rdoproject.org/) distribution and the OpenStack [RPM-Packaging](https://wiki.openstack.org/wiki/Rpm-packaging) project to produce a package for installation on our CentOS 7 controllers.
*   Configuration - Puppet provides us the configuration management for the CERN cloud. We are currently merging the [CERN Puppet EC2 API](https://github.com/cernops/puppet-ec2api) modules to the [puppet-ec2api](https://github.com/openstack/puppet-ec2api) project. The [initial patch](https://review.openstack.org/#/c/276103/) is now in review.
*   Monitoring - each new project has a set of daemons to make sure are running smoothly. These have to be integrated into the site monitoring system.
*   Performance - we use the [OpenStack Rally](https://wiki.openstack.org/wiki/Rally) project to continuously run functionality and performance tests, simulating a user. The EC2 support has been added in this [review](https://review.openstack.org/#/c/147550/).

The current steps are the end user testing and migration from the current service. Given that the ec2-api project can be run on a different port, the two services can be run in parallel for testing. Horizon would need to be modified to change the EC2 endpoint in the ec2rc.sh  (which is downloaded from Compute->Account & Security->API Access).  
  
So far, the tests have been positive and further validation will be performed over the next few months to make sure that the migration has completed so there is no impact on the Mitaka upgrade.  
  

### Acknowledgements

*   Wataru Takase (KEK) for his work on Rally
*   Marcos Fermin Lobo (CERN/Oviedo) for the packaging and configuration
*   Belmiro Moreira (CERN) for the necessary local CERN customisations in Nova
*   The folks from Cloudscaling/EMC for their implementation and support of the OpenStack EC2 API project

### References

*   CERN enduser documentation for EC2 at [http://clouddocs.web.cern.ch/clouddocs/ec2tutorial/index.html](http://clouddocs.web.cern.ch/clouddocs/ec2tutorial/index.html)

Notes
-----

Migrated from [OpenStack in Production Blog](http://openstack-in-production.blogspot.fr)

