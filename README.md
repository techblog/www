# CERN TechBlog

https://techblog.web.cern.ch/techblog/

## Install Hugo

Here are a few options to get the latest version of Hugo (recommended):

```bash
$ dnf install hugo

$ snap install hugo

$ brew install hugo

$ choco install hugo -confirm
```

For other platforms, [check here](https://gohugo.io/getting-started/installing/).

## Adding Content

Content generation is fully managed by Gitlab's CI/CD.

1. Checkout and prepare the repository
```bash
$ git clone https://:@gitlab.cern.ch:8443/techblog/www.git
$ cd www
$ git submodule update --init
```

2. Add new content (a new post), generating it locally first:
```bash
$ hugo new post/<name-of-the-post>.md
```

2. Edit the post under *content/post/name-of-the-post.md*. Some markdown help
[available here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).
If you need to add any images, keep them under *static/img/post*.

3. When happy to get reviews, create a branch and open a merge request against
the original repository:
```bash
$ git checkout -b name-of-the-post
$ git push name-of-the-post
```

4. Follow the link to create the MR, open it against the *master* branch

5. Follow up on the feedback given by other editors

## Reviewing Content

Each post is pushed as a new MR.

1. Once the builds succeeds, use the link in the MR to review the final content

2. Add comments and recommend changes as appropriate

3. Approve the post when happy

4. If you have the rights to do it, you can also merge the MR

The content will be automatically pushed, no manual step required.

## Content and Reviewing Tips

1. Reuse *tags* as much as possible, used for syndication

2. A picture is worth a thousand words

3. Add actual numbers as much as possible, results

4. Pick the title carefully, as well as the opening paragraph

5. Ask for feedback, we have a comment section

## Local Build and Review

You can run a local server, enabling draft post generation.
```
$ git clone https://:@gitlab.cern.ch:8443/techblog/www.git
$ git submodule update --init
$ hugo server -D
...
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

The local render will be available at http://localhost:1313/techblog

## Updating the theme

If the theme needs updating due to deprecation or new features, you
should update the submodule:

```
$ git clone https://:@gitlab.cern.ch:8443/techblog/www.git
$ cd www
$ git submodule update --remote
$ git commit themes/beautifulhugo -m 'Update beautifulhugo theme'
```
